<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//student
Route::apiResources([
    'student' => 'API\StudentController',
]);
Route::get('getStudentAtYear/{year}', 'API\StudentController@getStudentAtYear');
Route::get('studentAtFaculty', 'API\StudentController@studentAtFaculty');
Route::get('findStudentAtFaculty', 'API\StudentController@findStudentAtFaculty');
Route::put('updateStudentPretest/{st_id}', 'API\StudentController@confirmingRegister');
Route::get('admissionFormInfo/{st_id}', 'API\PretestController@admissionFormInfo');
Route::post('importStudentCode', 'API\StudentController@importStudentCode');
Route::get('findStudent', 'API\StudentController@findStudent');
Route::get('studentInfo/{id}', 'API\StudentController@studentInfo');
Route::put('updateStudentInfo/{st_id}', 'API\StudentController@updateStudentInfo');
Route::post('studentTakePhoto', 'API\StudentController@studentTakePhoto');

Route::apiResources([
    'register' => 'API\RegisterController',
]);
Route::get('bachelorRegisterInfo', 'API\RegisterController@bachelorRegisterInfo');
Route::get('studentClass', 'API\RegisterController@studentClass');

Route::apiResources([
    'faculty' => 'API\FakultyController',
]);

Route::apiResources([
    'department' => 'API\DepartmentController',
]);
Route::get('departmentAtFaculty/{ft_id}', 'API\DepartmentController@departmentAtFaculty');

Route::apiResources([
    'studentRegister' => 'API\StudentRegisterController',
]);
Route::post('findStudentRegister', 'API\studentRegisterController@findStudentRegister');
Route::post('updateStudentRegister', 'API\studentRegisterController@updateStudentRegister');
Route::post('findStudentRegisterAtClass', 'API\studentRegisterController@findStudentRegisterAtClass');
Route::post('currentStudent', 'API\studentRegisterController@currentStudent');//nama mahasiswa untuk bayar daftar ulang

Route::apiResources([
    'studentSubject' => 'API\StudentSubjectController',
]);
Route::post('updateStudentSubject', 'API\StudentSubjectController@updateStudentSubject');
Route::get('studentSubjectList/{st_id}', 'API\StudentSubjectController@studentSubject');
Route::put('updateStudentScore/{st_id}', 'API\StudentSubjectController@updateStudentScore');

Route::apiResources([
    'user' => 'API\UserController',
]);
Route::get('findUser', 'API\UserController@findUser');
//Route::get('user', 'API\UserController@index');
//Route::put('user/{id}/edit', 'API\UserController@update');

Route::apiResources([
    'pretest' => 'API\PretestController',
]);
Route::put('confirmingRegister/{id}', 'API\PretestController@confirmingRegister');
Route::get('cancelPretest/{st_id}', 'API\PretestController@cancelPretest');
Route::post('uploadPhoto', 'API\PretestController@takePhoto');
Route::get('findPretest', 'API\PretestController@findPretest');
Route::get('countRegister', 'API\PretestController@countRegister');
Route::get('admissionRegisterStatistic', 'API\PretestController@admissionRegisterStatistic');
Route::get('admissionTestRoomPretest', 'API\PretestController@admissionTestRoomPretest');
Route::get('admissionExaminerCount/{atr_id}', 'API\AdmissionPretestController@admissionExaminerCount');
Route::get('admissionPretestStudent/{room_id}', 'API\AdmissionPretestController@admissionPretestStudent');
Route::get('admissionPretestBiodataStudent/{room_id}', 'API\AdmissionPretestController@admissionPretestBiodataStudent');
Route::get('admissionStudent', 'API\PretestController@admissionStudent');
Route::get('findAdmissionStudent', 'API\PretestController@findAdmissionStudent');
Route::get('admissionStudentMuqaddimahForm/{st_id}', 'API\PretestController@admissionStudentMuqaddimahForm');
Route::get('currentStudentMuqaddimahPayed', 'API\PretestController@currentStudentMuqaddimahPayed');

Route::apiResources([
    'province' => 'API\ProvinceController',
]);

Route::apiResources([
    'amphur' => 'API\AmphurController',
]);

Route::apiResources([
    'district' => 'API\DistrictController'
]);

Route::apiResources([
    'admissionTestRoom' => 'API\AdmissionTestRoomController',
]);
Route::get('testRoomByGender/{gender}', 'API\AdmissionTestRoomController@testRoomByGender');

Route::apiResources([
    'admssionPretest' => 'API\AdmissionPretestController',
]);
Route::get('seatList/{atr_id}', 'API\AdmissionPretestController@seatList');
Route::get('seatCount/{atr_id}', 'API\AdmissionPretestController@seatCount');

Route::apiResources([
    'admissionRegister' => 'API\AdmissionRegisterController',
]);
Route::get('currentAdmissionRegister', 'API\AdmissionRegisterController@currentAdmissionRegister');

Route::apiResources([
    'muqaddimah' => 'API\MuqaddimahPayController',
]);

//Laporan
//Muqaddimah
Route::get('muqaddimahReportStatistic', 'API\ReportController@muqaddimahReportStatistic');
Route::get('muqaddimahPayedDate', 'API\ReportController@muqaddimahPayedDate');
Route::post('muqaddimahReportMoney', 'API\ReportController@muqaddimahReportMoney');
Route::post('findmuqaddimahReportMoneyDateRange', 'API\ReportController@findmuqaddimahReportMoneyDateRange');
//print mahasiswa
Route::get('currentClassList', 'API\ReportController@currentClassList');
Route::post('studentClassroom', 'API\ReportController@studentClassroom');
Route::post('studentClassroomExell', 'API\ReportController@studentClassroomExell');

//ulang_pays
Route::apiResources([
    'ulangPay' => 'API\UlangPayController',
]);
Route::post('ulangPayStudentPayed', 'API\UlangPayController@studentPayed');
Route::post('ulangPayStudentDelete', 'API\UlangPayController@ulangPayStudentDelete');
Route::post('ulangPayStudentDrop', 'API\UlangPayController@ulangPayStudentDrop');
Route::get('UlangPayStudentDropList', 'API\UlangPayController@UlangPayStudentDropList');
Route::delete('UlangPayStudentDropDelete/{id}', 'API\UlangPayController@UlangPayStudentDropDelete');

//Drop
Route::apiResources([
    'drop' => 'API\DropController',
]);
Route::get('findDropStudent', 'API\DropController@findDropStudent');

//Mustawa data
Route::apiResources([
    'mustawadata' => 'API\MustawaDataController'
]);

//Mustawa_register
Route::apiResources([
    'mustawaRegister' => 'API\MustawaRegisterController'
]);
Route::post('mustawaRegisterCancel', 'API\MustawaRegisterController@cancelPayment');
Route::get('mustawaRegisterData/{id}', 'API\MustawaRegisterController@mustawaRegisterData');
Route::post('mustawaMoneyReport', 'API\MustawaRegisterController@moneyReport');
Route::post('mustawaMoneyStatistic', 'API\MustawaRegisterController@mustawaMoneyStatistic');

//student_register
Route::apiResources([
    'studentRegister' => 'API\studentRegisterController',
]);
Route::get('studentRegisterSearch', 'API\studentRegisterController@studentRegisterSearch');
Route::post('yuranPaymentInfo', 'API\studentRegisterController@yuranPaymentInfo');

//payments
Route::apiResources(['payment' => 'API\PaymentController']);
Route::post('yuranCancelPay', 'API\PaymentController@yuranCancelPay');




