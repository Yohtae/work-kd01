<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';

    public function faculty(){
        return $this->belongsTo('App\Fakulty','ft_id','ft_id');
    }

}
