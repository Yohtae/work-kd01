<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fakulty extends Model
{
    protected $table = 'fakultys';

    public function department(){
        return $this->hasMany('App\Department');
    }
}
