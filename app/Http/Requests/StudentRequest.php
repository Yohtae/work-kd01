<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($this->method() == 'PATH' || $this->method() == 'PUT'){
            $cityzen_id = 'required|numeric|digits:13|unique:students,cityzen_id,'.$request->student.',st_id';
        }else{
            $cityzen_id = 'required|numeric|digits:13';
        }

        return [
            'cityzen_id' => $cityzen_id,
            'firstname_rumi' => 'required|string',
            'lastname_rumi' => 'required|string',
            't_studentname' => 'required|string',
            't_studentlastname' => 'required|string',
            'firstname_jawi' => 'required|string',
            'lastname_jawi' => 'required|string',
            'gender' => 'required',
            'birdth_date' => 'required',
            'father_name' => 'required|string',
            'father_lastname' => 'required|string',
            'mother_name' => 'required|string',
            'mother_lastname' => 'required|string',
            'familyStatus' => 'required',
            't_fathername' => '',
            't_fatherlastname' => '',
            't_mothername' => '',
            't_motherlastname' => '',
            'disease' => '',
            'telephone' => '',
            'email' => '',
            't_village_name' => 'required',
            'house_number' => 'required|string',
            'place' => 'required|numeric',
            't_road' => '',
            't_province_sec' => '',
            't_higschool' => '',
            't_relegion' => '',
            't_province' => 'required',
            't_district' => 'required',
            't_subdistrict' => 'required',
            'post' => 'required',
            'ibtidai_graduate_year' => '',
            'ibtidaiVillage' => 'required',
            'ibtidai_graduate' => 'required',
            'mutawasit_graduate_year' => '',
            'mutawasit_graduate' => 'required',
            'mutawassitVillage' => 'required',
            'sanawi_graduate' => 'required',
            'sanawiVillage' => 'required',
            'sanawi_graduate_year' => '',
            'down_graduate' => '',
            'down_graduate_year' => '',
            'first_highschool_graduate' => '',
            'first_highschool_graduate_year' => '',
            'second_highschool_graduate' => '',
            'second_highschool_graduate_year' => '',
            'other' => '',
            'income_year' => '',
            'class' => '',
            //'register_date' => '',
            'username' => '',
            'password' => '',
            'image' => '',
            //'last_update' => '',
            'melayu_lang_skill' => 'required',
            'arab_lang_skill' => 'required',
            'ingris_lang_skill' => 'required',
            'thai_lang_skill' => 'required',
            'certificate' => 'required',
            'citizen_book' => 'required',
            'id_book' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'cityzen_id.required' => 'di perlukan',
                'cityzen_id.digits' => 'harus 13 huruf',
                'cityzen_id.numeric' => 'harus anko sahaja',
                'cityzen_id.unique' => 'anda sudah daftar',
                'firstname_rumi.required' => 'di perlukan',
                'lastname_rumi.required' => 'di perlukan',
                't_studentname.required' => 'di perlukan',
                't_studentlastname.required' => 'di perlukan',
                'firstname_jawi.required' => 'di perlukan',
                'lastname_jawi.required' => 'di perlukan',
                'gender.required' => 'di perlukan',
                'birdth_date.required' => 'di perlukan',
                'father_name.required' => 'di perlukan',
                'father_lastname.required' => 'di perlukan',
                'mother_name.required' => 'di perlukan',
                'mother_lastname.required' => 'di perlukan',
                'familyStatus.required' => 'di perlukan',
                'house_number.required' => 'di perlukan',
                't_village_name.required' => 'di perlukan',
                'place.required' => 'di perlukan',
                'place.numeric' => 'taip angko sahaja',
                't_province.required' => 'di perlukan',
                't_district.required' => 'di perlukan',
                't_subdistrict.required' => 'di perlukan',
                'postcode.required' => 'di perlukan',
                'ibtidai_graduate.required' => 'di perlukan',
                'ibtidaiVillage.required' => 'di perlukan',
                'mutawasit_graduate.required' => 'di perlukan',
                'mutawassitVillage.required' => 'di perlukan',
                'sanawi_graduate.required' => 'di perlukan',
                'sanawiVillage.required' => 'di perlukan',
                'melayu_lang_skill.required' => 'di perlukan',
                'arab_lang_skill.required' => 'di perlukan',
                'ingris_lang_skill.required' => 'di perlukan',
                'thai_lang_skill.required' => 'di perlukan',
                'certificate.required' => 'di perlukan',
                'citizen_book.required' => 'di perlukan',
                'id_book.required' => 'di perlukan',
        ];
    }
}
