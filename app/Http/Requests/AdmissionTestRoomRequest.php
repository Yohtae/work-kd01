<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdmissionTestRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == "PATH" || $this->method() == "PUT"){
            $room_name = 'required|unique:admission_test_rooms,room_name,'.$this->get('id');
        }else{
            $room_name = 'required|unique:admission_test_rooms';
        }

        return [
            'room_name' => $room_name,
            'room_gender' => 'required',
        ];
    }

    public function messages(){
        return [
            'room_name.required' => 'di perlukan',
            'room_name.unique' => 'data sudah ada',
            'room_gender.required' => 'di perlukan', 
        ];
    }
}
