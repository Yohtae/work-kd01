<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdmissionRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == "PATH" || $this->method() == "PUT"){
            $ar_year = 'required|numeric|digits:4|unique:admission_registers,ar_year,'.$this->get('id');
        }else{
            $ar_year = 'required|numeric|digits:4|unique:admission_registers,ar_year';
        }

        return [
            'ar_year' => $ar_year,  
            'ar_start_date' => 'required',
            'ar_end_date' => 'required',
            'status' => 'required', 
            'ar_register_cost' => 'required|numeric',
            'ar_muqaddimah_cost' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'ar_year.required' => 'di perlukan',
            'ar_year.numeric' => 'taip tahun',
            'ar_year.digits' => 'taip tahun',
            'ar_year.unique' => 'data sudah ada',
            'ar_start_date.required' => 'di perlukan',
            'ar_end_date.required' => 'di perlukan',
            'status.required' => 'di perlukan',
            'ar_register_cost.required' => 'di perlukan',
            'ar_register_cost.numeric' => 'taip jumlah duit',
            'ar_muqaddimah_cost.required' => 'di perlukan',
            'ar_muqaddimah_cost.numeric' => 'taip jumlah duit',
        ];
    }
}
