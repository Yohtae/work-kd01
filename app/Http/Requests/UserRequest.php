<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATH' || $this->method() == 'PUT'){
            $idcard = 'required|numeric|digits:13|unique:users,idcard,'.$this->get('id').',id';
            $email = 'required|string|email|max:191|unique:users,email,'.$this->get('id').',id';
            $password = 'string|min:8';
            $username = 'required|string|min:3|unique:users,username,'.$this->get('id').',id';
        }else{
            $idcard = 'required|numeric|digits:13|unique:users';
            $email = 'required|string|email|max:191|unique:users';
            $password = 'required|string|min:8';
            $username = 'required|string|min:3|unique:users';
        }

        return [
            'name' => 'required|string|max:191',
            'lastname' => 'required|string|max:191',
            'idcard' => $idcard,
            'telephone' => 'required|numeric|digits:10',
            'type' => 'required|integer',
            'email' => $email,
            'username' => $username,
            'password' => $password,   
        ];
    }
}
