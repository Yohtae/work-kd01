<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class studentRegisterUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class' => 'required',
            're_id' => 'required',
            'ft_id' => 'required',
            //'dp_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'class.required' => 'Pilih tahun daftar',
            're_id.required' => 'Pilih tahun pengajian',
            'ft_id.required' => 'Pilih fakulti',
            //'dp_id.required' => 'Pilih jurusan',
        ];
    }
}
