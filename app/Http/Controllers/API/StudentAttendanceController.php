<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentAttendance;

class StudentAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //jumlah dan tarikh berlaku perkuliahan
    public function totalStudied($subjects,$class,$ft_id,$dp_id,$term,$year,$students)
    {
        foreach($subjects as $subject){
            $s_id = $subject->s_id;
            $att[] = StudentAttendance::join('student_register', 'student_register.st_id', '=', 'student_attendance.st_id')
                                ->join('students', 'students.st_id', '=', 'student_attendance.st_id')
                                ->where('student_register.sr_class',$class)
                                ->where('student_register.term',$term)
                                ->where('student_register.academic_year',$year)
                                ->where('students.ft_id',$ft_id)
                                ->where('students.dp_id',$dp_id)
                                ->where('s_id',$s_id)
                                ->groupBy('sa_date')
                                ->get();
        }    

        $totalDate = 0;
        foreach($att as $date){
            foreach($date as $day){
                $totalDate = $totalDate + 1;
                $value[] =  array(
                    's_id' => $day->s_id,
                    'studyDate' => $day->sa_date,
                );   
            }
        }

        //mencari hari yang mahasiswa tidak hadir
        foreach($students as $student){
            $st_id = $student->st_id;
            $totalStudentCome = 0;
            foreach($value as $absentDate){
                $s_id = $absentDate['s_id'];
                $sa_date = $absentDate['studyDate'];
                $studentCome = StudentAttendance::where('st_id',$st_id)
                                ->where('s_id',$s_id)
                                ->where('sa_date',$sa_date)
                                ->count();
                $totalStudentCome = $totalStudentCome + $studentCome;
            }
            $totalAbsent = $totalDate - $totalStudentCome;
            $absentPercent = (100*$totalAbsent)/$totalDate;
            $studentAttendanceInfo[] = array(
                'st_id' => $st_id,
                'student_id' => $student->student->student_id,
                'firstname_rumi' => $student->student->firstname_rumi,
                'lastname_rumi' => $student->student->lastname_rumi,
                'totalStudentCome' => $totalStudentCome,  
                'totalAbsent' => $totalAbsent,
                'absentPercent' => $absentPercent,
            );
        }

        $collection = collect($studentAttendanceInfo);
        $sorted = $collection->SortByDesc('totalAbsent');
        $studentWithAbsent = $sorted->values()->all();
             
        return array([
            'studentWithAbsent' => $studentWithAbsent,
            'totalData' => $totalDate,//jumlah hari berlaku perkuliahan
            'studyDate' => $value,//tarikh berlaku perkuliahan
        ]);
    } 
}
