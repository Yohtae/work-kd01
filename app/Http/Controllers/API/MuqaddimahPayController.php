<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AdmissionRegisterController;
use App\Http\Controllers\API\RegisterController;
use App\Image;
use App\MuqaddimahPay;
use App\Student;
use App\Pretest;

class MuqaddimahPayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        //authorization
        //$this->authorize('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AdmissionRegisterController $admissionRegisterController, RegisterController $registerController, FakultyController $faculty, DepartmentController $department)
    {

        if($request->ft_id == '121' || $request->ft_id == '125'){
            if($request->category == '0'){
                $rules = [
                    'category' => 'required',
                    'ft_id' => 'required',
                    'dp_id' => 'required',
                ];
            }else{
                $rules = [
                    'category' => 'required',
                    'class' => 'required',
                    'ft_id' => 'required',
                    'dp_id' => 'required',
                ];
            }
            $dp_id = $request->dp_id;
        }else{
            if($request->category == '0'){
                $rules = [
                    'ft_id' => 'required',
                    'category' => 'required',
                ];
            }else{
                $rules = [
                    'category' => 'required',
                    'class' => 'required',
                    'ft_id' => 'required',
                ];
            }
            $dp_id = 0;
        }

        $messages = [
            'ft_id.required' => 'di perlukan',
            'dp_id.required' => 'di perlukan',
            'class.required' => 'di perlukan',
            'category.required' => 'di perlukan',
        ];

        $request->validate($rules, $messages);
        $register = $registerController->currentAcademicYear();
        $currentYear = $register['re_year'];
        $user_id = auth('api')->user()->id;

        //update table pretest --> type 0:normal 2:transfer
        Pretest::where('st_id', $request->st_id)->update([
            'type' => $request->category,
            'user_id' => $user_id
        ]);

        //insert data to table muqaddimah_pay
        $m_academicyear = $admissionRegisterController->currentAdmissionRegister();
        //m_reciet 
        //count m_reciet in year
        $count = MuqaddimahPay::where('m_academicyear', $m_academicyear->ar_year)->count();
        //generate m_reciet code
        if($count == 0){
            $m_number = 1;
            $recietNumber = 'M'.substr($m_academicyear->ar_year,2).'/'.str_pad(1, 4, "0", STR_PAD_LEFT);
        }else{
            $allNumber = MuqaddimahPay::where('m_academicyear', $m_academicyear->ar_year)->get(['m_number']);

            $collection = collect($allNumber);
            $sorted = $collection->SortBy('m_number');
            $numbers = $sorted->values()->all();

            $i = 1;
            foreach($numbers as $number){
                $number = $number->m_number;
                if($i != $number){
                    $m_number = $i;
                    $recietNumber = 'M'.substr($m_academicyear->ar_year,2).'/'.str_pad($m_number, 4, "0", STR_PAD_LEFT);
                    break;
                }else{
                    $m_number = $number+1;
                    $recietNumber = 'M'.substr($m_academicyear->ar_year,2).'/'.str_pad($i+1, 4, "0", STR_PAD_LEFT);
                }
                $i++;
            }
        }

        //insert data to table muqaddimah_pay
        MuqaddimahPay::create([
            'p_id' => $request->p_id,
            'st_id' => $request->st_id,
            'm_academicyear' => $m_academicyear->ar_year,
            'm_paydate' => date('Y-m-d'),
            'm_money' => $m_academicyear->ar_muqaddimah_cost,
            'm_reciet' => $recietNumber,
            'm_number' => $m_number,
            'user_id' => $user_id
        ]);

        //update data table students
        if($request->category == 0){
            $class = $currentYear;
            $program = 155;
        }else{
            $class = $request->class;
            $program = NULL;
        }

        $pretest = Pretest::where('pre_id', $request->p_id)->first(['type','odrNumber','std_photo']);
        $muqaddimah = MuqaddimahPay::where('p_id', $request->p_id)->first();
        $student = Student::where('st_id', $request->st_id)->first(['firstname_rumi','lastname_rumi','ft_id','dp_id']);

        Student::where('st_id', $request->st_id)->update([
            'ft_id' => $request->ft_id,
            'dp_id' => $dp_id,
            'income_year' => $currentYear,
            'class' => $class,
            'program' => $program,
            'photo' => $pretest['std_photo']
        ]);

        if(Image::where('st_id',  $request->st_id)->exists() == false){
            //$image = Image::where('st_id',  $request->st_id)->first();
            Image::create(['images' => $pretest['std_photo'], 'name' => 'images/'.$pretest['std_photo'], 'st_id' => $request->st_id]);
        }else{
            Image::where('st_id', $request->st_id)->update(['images' => $pretest['std_photo'], 'name' => 'images/'.$pretest['std_photo']]);
        }

        $ft_name = $faculty->show($request->ft_id);
        $dp_name = $department->show($request->dp_id);

        return [
            //'allNumber' => $numbers,
            //'value' => $value,
            //'formData' => $request->all(),
            // 'm_academicyear' => $m_academicyear->ar_year,
            // 'ar_muqaddimah_cost' => $m_academicyear->ar_muqaddimah_cost,
            // 'm_paydate' => date('Y-m-d'),
            // 'count' => $count,
            // 'user_id' => $user_id,
            // 'ft_id' => $request->ft_id,
            // 'dp_id' => $dp_id,
            'data' => $request->all(),
            'class' => $class,
            'faculty' => $ft_name['ft_name'],
            'department' => $dp_name['dp_name'],
            'muqaddimah' => $muqaddimah,
            'student' => $student,
            'pretest' => $pretest,
            'type' => $pretest['type'], //status bayar muqaddimah
            'status' => 'success',
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $muqaddimah = MuqaddimahPay('p_id', $id)->first();
        return $muqaddimah;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MuqaddimahPay::where('p_id', $id)->delete();
        Pretest::where('pre_id', $id)->update([
            'type' => null
        ]);
        $pretest = Pretest::where('pre_id', $id)->first(['type','st_id']);
        Student::where('st_id', $pretest->st_id)->update(['income_year' => '', 'class' => '', 'program' => NULL]);
        return [
            'type' => $pretest['type'], //status bayar muqaddimah
            'status' => 'success',
        ];
    }
}
