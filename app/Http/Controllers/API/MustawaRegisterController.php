<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\MustawaDataController;
use Illuminate\Support\Facades\Hash;
use App\MustawaData;
use App\MustawaRegister;
use App\Student;


class MustawaRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        //
    }

    
    public function store(Request $request, MustawaDataController $mustawaDataController)
    {
        $rules = [
            'mustawadata_id' => 'required',
            'learningGroup' => 'required',
        ];

        $messages = [
            'mustawadata_id.required' => 'di perlukan',
            'learningGroup.required' => 'di perlukan', 
        ];

        //gererate reciet code
        $mustawaData = $mustawaDataController->show($request->mustawadata_id);
        $year = $mustawaData['year'];
        $recietCount = MustawaRegister::where('year', $year)->count();
        //$allReciet = MustawaRegister::where('year', $year)->orderBy('reciet')->get();

        if($recietCount == 0){
            $reciet = 1;
            $reciet_code = 'L'.substr($year,2).'/'.str_pad(1, 4, "0", STR_PAD_LEFT);
        }else{
            $numbers = MustawaRegister::where('year', $year)->orderBy('reciet')->get();
            $i = 1;
            foreach($numbers as $number){
                $number = $number->reciet;
                if($i != $number){
                    $reciet = $i;
                    $reciet_code = 'L'.substr($year,2).'/'.str_pad($reciet, 4, "0", STR_PAD_LEFT);
                    break;
                }else{
                    $reciet = $number+1;
                    $reciet_code = 'L'.substr($year,2).'/'.str_pad($reciet, 4, "0", STR_PAD_LEFT);
                }
                $i++;
            }
        }

        $request->validate($rules, $messages);

        $data = array(
            'mustawadata_id' => $request->mustawadata_id,
            'payMoney' => $mustawaDataController->show($request->mustawadata_id)->prize,
            'register_date' => date('Y-m-d'),
            'reciet' => $reciet,
            'reciet_code' => $reciet_code,
            'st_id' => $request->st_id,
            'learningGroup' => $request->learningGroup,
            'learningStatus' => 0,
            'year' => $mustawaDataController->show($request->mustawadata_id)->year,
        );

        $exist = MustawaRegister::where('st_id', $request->st_id)->where('mustawadata_id', $request->mustawadata_id)->exists();

        if($exist == true){
            $status = 'exist';
        }else{
            MustawaRegister::create($data);
            $status = 'success';
        }

        return [
            'status' => $status,
            //'data' => $data,
        ];

    }

    public function show($id)
    {
        $data = MustawaRegister::join('mustawadata', 'mustawa_register.mustawadata_id', '=', 'mustawadata.mustawaData_id')
                                ->where('mustawa_register.st_id', $id)
                                ->get();
        return $data;
    }

    public function update(Request $request, $id)
    {
        $rules = [
            //'mustawadata_id' => 'required',
            'learningGroup' => 'required',
        ];

        $messages = [
            //'mustawadata_id.required' => 'di perlukan',
            'learningGroup.required' => 'di perlukan', 
        ];

        $request->validate($rules, $messages);

        MustawaRegister::where('mustawa_register_id', $id)->update(['learningGroup' => $request->learningGroup]);

        return $request->all();
    }

    public function destroy($id)
    {
        return $id;
    }

    public function cancelPayment(Request $request)
    {

        $rules = [
            'password' => 'required',
        ];

        $messages = [
            'password.required' => 'di perlukan',
            //'password.match' => 'password salah',
        ];

        $hashedPassword = $user = auth('api')->user()->getAuthPassword();
        $password = $request->password;

        if(Hash::check($password, $hashedPassword)) {
            $status = 'match';
            MustawaRegister::where('mustawa_register_id', $request->mustawa_register_id)->delete();
        }else{
            $status = 'not';
        }

        $request->validate($rules, $messages);

        return [
            'status' => $status,
            'data' => $request->all(),
        ];
    }

    public function mustawaRegisterData($id)
    {
        $data = MustawaRegister::where('mustawa_register_id', $id)->first();
        return $data;
    }

    //laporan duit mustawa
    public function moneyReport(Request $request)
    {
        //\sleep(2);
        $today = date('Y-m-d');
        if($request->searchType == 'today'){
            $totalMoney = MustawaRegister::where('register_date', $today)->sum('payMoney');
            $payList = MustawaRegister::join('mustawadata', 'mustawa_register.mustawadata_id', '=', 'mustawadata.mustawadata_id')
                                    ->join('students', 'mustawa_register.st_id', '=', 'students.st_id')
                                    ->where('mustawa_register.register_date', $today)
                                    ->select(['students.student_id','students.firstname_rumi','students.lastname_rumi',
                                            'mustawa_register.reciet_code','mustawa_register.payMoney','mustawa_register.created_at',
                                            'mustawadata.level','mustawadata.year'])
                                    ->orderBy('mustawa_register.reciet')
                                    ->get();
            $today = date('d/m/Y');
        }else{

            if($request->mustawadata_id == 'all'){
                $totalMoney = MustawaRegister::whereBetween('register_date', ([$request->startDate, $request->toDate]))
                                    ->sum('payMoney');
                $payList = MustawaRegister::join('mustawadata', 'mustawa_register.mustawadata_id', '=', 'mustawadata.mustawadata_id')
                                    ->join('students', 'mustawa_register.st_id', '=', 'students.st_id')
                                    ->whereBetween('mustawa_register.register_date', ([$request->startDate, $request->toDate]))
                                    ->select(['students.student_id','students.firstname_rumi','students.lastname_rumi',
                                            'mustawa_register.reciet_code','mustawa_register.payMoney','mustawa_register.created_at',
                                            'mustawadata.level','mustawadata.year','mustawa_register.register_date'])
                                    ->orderBy('mustawa_register.reciet')
                                    ->get();
            }else{
                $totalMoney = MustawaRegister::where('mustawadata_id', $request->mustawadata_id)
                                        ->whereBetween('register_date', ([$request->startDate, $request->toDate]))
                                        ->sum('payMoney');
                $payList = MustawaRegister::join('mustawadata', 'mustawa_register.mustawadata_id', '=', 'mustawadata.mustawadata_id')
                                        ->join('students', 'mustawa_register.st_id', '=', 'students.st_id')
                                        ->where('mustawa_register.mustawadata_id', $request->mustawadata_id)
                                        ->whereBetween('mustawa_register.register_date', ([$request->startDate, $request->toDate]))
                                        ->select(['students.student_id','students.firstname_rumi','students.lastname_rumi',
                                                'mustawa_register.reciet_code','mustawa_register.payMoney','mustawa_register.created_at',
                                                'mustawadata.level','mustawadata.year','mustawa_register.register_date'])
                                        ->orderBy('mustawa_register.reciet')
                                        ->get();
            }

           // $today = "Dari ".date_format('d-m-Y', $request->startDate)." Hingga ".$request->toDate;
            $today = date('d', strtotime($request->startDate))." - ".date('d/m/Y', strtotime($request->toDate));
        }

        return [
            'request' => $request->all(),
            'totalMoney' => number_format($totalMoney),
            'payList' => $payList,
            'today' => $today,
        ];
    }

    //statistic pendapatan mustawa
    public function mustawaMoneyStatistic(Request $request)
    {
        $mustawaData = MustawaData::orderBy('year', 'DESC')->limit(20)->get();

        foreach($mustawaData as $item){
            $totalMoney = MustawaRegister::where('mustawadata_id', $item->mustawaData_id)->sum('payMoney');
            $student = MustawaRegister::where('mustawadata_id', $item->mustawaData_id)->count();
            $statistic[] = array(
                'level' => $item->level,
                'year' => $item->year,
                'prize' => $item->prize,
                'totalMoney' => number_format($totalMoney),
                'student' => $student
            );
        }


        return [
            'status' => 'success',
            'statistic' => $statistic
        ];
    }

}
