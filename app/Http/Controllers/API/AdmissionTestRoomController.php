<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdmissionTestRoom;
use App\Http\Requests\AdmissionTestRoomRequest;
use App\AdmissionRegister;

class AdmissionTestRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admissionTestRooms = AdmissionTestRoom::with('admissionRegister')
                                ->whereHas('admissionRegister', function($q){
                                    $q->where('status',1);
                                })
                                ->get();
        return $admissionTestRooms;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdmissionTestRoomRequest $request)
    {
        $exist = AdmissionRegister::where('status', 1)->exists();
        if($exist == true){
            $ar_id = AdmissionRegister::where('status', 1)->first()->id;
            AdmissionTestRoom::create([
                'ar_id' => $ar_id,
                'room_name' => $request->room_name,
                'room_gender' => $request->room_gender,
                'room_description' => $request->room_description,
            ]);
            $status = 'success';
        }else{
            $status = 'error';
        }
        return [
            // 'ar_id' => $ar_id,
            // 'exist' => $exist,
            'status' => $status,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $atr_id = AdmissionTestRoom::find($id);
        $atr_id->delete();
        return ['success'];
    }

    //bilik periksa mengikut jenis kelamin
    public function testRoomByGender($gender)
    {
        if($gender == "Lelaki"){
            $genderId = 1;
        }else{
            $genderId = 2;
        }
        $admissionTestRooms = AdmissionTestRoom::where('room_gender','=',$genderId)
                                ->with('admissionRegister')
                                ->whereHas('admissionRegister', function($q){
                                    $q->where('status',1);
                                })
                                ->get(); 
        return $admissionTestRooms;
    }
    //bilik periksa tahun yang buka daftar sekarang
    public function currentTestRoom($ar_id)
    {
        $rooms = AdmissionTestRoom::where('ar_id',$ar_id)->get();
        return $rooms;
    }
    //bilik periksa tahun yang buka daftar sekarang mengikut jenis kelamin
    public function currentTestRoomByGender($ar_id,$gender)
    {
        $rooms = AdmissionTestRoom::where('ar_id',$ar_id)
                    ->where('room_gender',$gender)
                    ->get();
        return $rooms;
    }
}
