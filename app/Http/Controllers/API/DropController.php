<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Pretest;
use App\UlangPay;
use App\MuqaddimahPay;
use App\Image;
use App\Http\Controllers\API\FakultyController;
use App\Http\Controllers\API\DepartmentController;

class DropController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //mahasiswa yang daftar drop
    public function index()
    {
        $students = MuqaddimahPay::join('students', 'muqaddimah_pay.st_id', '=', 'students.st_id')
                                ->orderBy('students.student_id', 'DESC')
                                ->select([
                                    'students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id','students.cityzen_id'
                                ])
                                ->paginate(20);
        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, FakultyController $faculty, DepartmentController $department)
    {
        $student = MuqaddimahPay::join('students', 'muqaddimah_pay.st_id', '=', 'students.st_id')
                        ->where('students.st_id', $id)
                        ->select([
                            'students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id',
                            'students.cityzen_id','ft_id','dp_id'
                        ])
                        ->first();
        $image = Image::where('st_id', $id)->first();
        $faculty = $faculty->show($student['ft_id']);
        $department = $department->show($student['dp_id']);

        return [
            'status' => 'success',
            'data' => $student,
            'photo' => $image['images'],
            'faculty' => $faculty['ft_name'],
            'department' => $department['dp_short_name'],
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function findDropStudent(Request $request)
    {
        $q = $request->q;
        $students = MuqaddimahPay::join('students', 'muqaddimah_pay.st_id', '=', 'students.st_id')
                                ->where('students.firstname_rumi', 'LIKE', '%'.$q.'%')
                                ->orWhere('students.student_id', 'LIKE', '%'.$q.'%')
                                ->orWhere('students.lastname_rumi', 'LIKE', '%'.$q.'%')
                                ->orWhere('students.firstname_jawi', 'LIKE', '%'.$q.'%')
                                ->orWhere('students.lastname_jawi', 'LIKE', '%'.$q.'%')
                                ->orWhere('students.cityzen_id', 'LIKE', '%'.$q.'%')
                                ->orderBy('students.student_id')
                                ->select([
                                    'students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id','students.cityzen_id'
                                ])
                                ->paginate(20);
        return $students;
    }
}
