<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\studentRegisterUpdateRequest;
//use App\Student;
use App\StudentRegister;
use App\Pretest;
use App\StudentRegisterExam;
use App\UlangPay;
use App\Http\Controllers\API\RegisterSubjectController;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\StudentAttendanceController;
use App\Http\Controllers\API\StudentRegisterExamController;
use App\Http\Controllers\API\AdmissionRegisterController;
use App\Payment;
use App\Register;

//use App\Http\Requests\StudentRequest;

class studentRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        //authorization
        //$this->authorize('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $students = StudentRegister::with(['student','student.faculty'])
                        ->orderBy('sr_id', 'DESC')
                        ->paginate(20);
        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //search mahasiswa yang daftar bayar yuran
    public function studentRegisterSearch(Request $request)
    {
        $q = $request->q;
        $students = StudentRegister::with(['student','student.faculty'])
                        ->whereHas('student', function($query) use ($q){
                            $query->whereNotNull('student_id');
                            $query->where('student_id', $q);
                            $query->orWhere('cityzen_id', 'LIKE', '%'.$q.'%');
                            $query->orWhere('firstname_rumi', 'LIKE', '%'.$q.'%');
                            $query->orWhere('lastname_rumi', 'LIKE', '%'.$q.'%');
                            $query->orWhere('firstname_jawi', 'LIKE', '%'.$q.'%');
                            $query->orWhere('lastname_jawi', 'LIKE', '%'.$q.'%');
                        })
                        ->orderBy('sr_id', 'DESC')
                        ->paginate(20);
        return $students;
    }

    public function findStudentRegister(StudentRegisterUpdateRequest $request)
    {
        if($request['dp_id']==null){
            $dp_id = '0';
        }else{
            $dp_id = $request['dp_id'];
        }
        $class = $request['class'];
        $re_id = $request['re_id'];
        $ft_id = $request['ft_id'];
        $studentRegister = StudentRegister::where('re_id',$re_id)
                            ->with('student')
                            ->whereHas('student', function($query) use ($class,$ft_id,$dp_id){
                                $query->where('class',$class);
                                $query->where('ft_id',$ft_id);
                                $query->where('dp_id',$dp_id);
                            })
                            ->get();
        $collection = collect($studentRegister);
        $sorted = $collection->SortBy('student.student_id');
        return $sorted->values()->all();
    }

    public function updateStudentRegister(StudentRegisterUpdateRequest $request)
    {
        if($request['dp_id']==null){
            $dp_id = '0';
        }else{
            $dp_id = $request['dp_id'];
        }
        $class = $request['class'];
        $re_id = $request['re_id'];
        $ft_id = $request['ft_id'];
        $sr_class = $request['sr_class'];
        $students = $this->findStudentRegister($request);
        foreach($students as $student){
            StudentRegister::where('sr_id',$student->sr_id)->update(array('sr_class' => $sr_class));
        }
        return $request->all();
    }

    public function findStudentRegisterAtClass(Request $request,
                                                RegisterSubjectController $RegisterSubjectController,
                                                RegisterController $RegisterController,
                                                StudentAttendanceController $StudentAttendanceController)
    {
        $ft_id = $request['ft_id'];
        if($request['dp_id']==null){
            $dp_id = '0';
        }else{
            $dp_id = $request['dp_id'];
        }
        $students = StudentRegister::with('student')
                    ->whereHas('student', function($q) use ($ft_id,$dp_id){
                        $q->where('ft_id',$ft_id);
                        $q->where('dp_id',$dp_id);
                    })
                    ->where('re_id',$request->re_id)
                    ->where('sr_class',$request->sr_class)
                    ->get();
        $collection = collect($students);
        $sorted = $collection->SortBy('student.student_id');
        $totalStudentInclass = $sorted->values()->all();

        //matapelajaran semua pada semester/tahun
        $ft_id = $ft_id;
        $dp_id = $dp_id;
        $rs_class = $request->sr_class;
        $rs_term = $RegisterController->findBachelorRegister($request->re_id)->re_term;
        $rs_academic_year = $RegisterController->findBachelorRegister($request->re_id)->re_year;
        $subjects = $RegisterSubjectController->registerSubjectAtSemesterYear($ft_id,$dp_id,$rs_class,$rs_term,$rs_academic_year);

        //jumlah hari dan tarikh berlaku perkuliahan 
        $studyDate = $StudentAttendanceController->totalStudied($subjects,$rs_class,$ft_id,$dp_id,$rs_term,$rs_academic_year,$totalStudentInclass);

        return [
            'studentWithAbsent' => $studyDate[0]['studentWithAbsent'],
            'totalStudent' => $totalStudentInclass,
            'totalStudyDate' => $studyDate[0]['totalData'],
        ];
    }

    //nama mahasiswa untuk bayar daftar ulang
    public function currentStudent(Request $request, 
                                    RegisterController $registerController, 
                                    StudentRegisterExamController $studentRegisterExamController, 
                                    AdmissionRegisterController $admissionRegisterController
                                    )
    {
        //data from post
        $ft_id = $request->ft_id;
        $dp_id = $request->dp_id;
        $class = $request->class;

        //tahun pererimaan mahasiswa baru
        $admissionRegisterYear = $admissionRegisterController->currentAdmissionRegister()->ar_year;

        //tahun pengajian sekarang
        $currentAcademicYear = $registerController->currentAcademicYear()->re_year;
        
        //semester & tahun sebelum terakhir yang daftar
        $beforCurrentAcademicYear = $currentAcademicYear-1;
        
        //mahasiswa yang dapat periksa pada semester & tahun sebelum terakhir
        //$studentCanExam = $studentRegisterExamController->studentCanExam('2', $beforCurrentAcademicYear, $ft_id, $dp_id, $class);

        //nama mahasiswa yang dapat periksa pada semester & tahun sebelum terakhir untuk daftar ulang
        $studentCanExam = StudentRegisterExam::join('student_register', 'student_register_exam.st_id', '=', 'student_register.st_id')
                            ->join('students', 'student_register_exam.st_id', '=', 'students.st_id')
                            ->where('student_register.pay_status', 'SUDAH LUNAS')
                            ->where('student_register_exam.pay_status', 'SUDAH LUNAS')
                            ->where('student_register_exam.year', $beforCurrentAcademicYear)
                            ->where('student_register_exam.term', 2)
                            ->where('student_register.academic_year', $beforCurrentAcademicYear)
                            ->where('student_register.term', 2)
                            ->where('students.ft_id', $ft_id)
                            ->where('students.dp_id', $dp_id)
                            ->where('students.class', $class)
                            ->select([
                                'students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id'
                            ])
                            ->orderBy('students.student_id')
                            ->get();
        $normalStudents = [];
        foreach($studentCanExam as $student){
            $count = UlangPay::where('st_id', $student->st_id)->where('up_year', $admissionRegisterYear)->exists(); 
            if($count == true){

            }else{
                $normalStudents[] = array(
                    'st_id' => $student->st_id,
                    'firstname_rumi' => $student->firstname_rumi,
                    'lastname_rumi' => $student->lastname_rumi,
                    'firstname_jawi' => $student->firstname_jawi,
                    'lastname_jawi' => $student->lastname_jawi,
                    'student_id' => $student->student_id
                );
            }
        }
        
        //mahasiswa transfer 
        $studentTransfer = Pretest::join('muqaddimah_pay', 'pretest.pre_id', '=', 'muqaddimah_pay.p_id')
                                    ->join('students', 'pretest.st_id', '=', 'students.st_id')
                                    ->where('pretest.type', '1')
                                    ->where('pretest.pre_register_year', $admissionRegisterYear)
                                    ->where('students.class', $class)
                                    ->where('students.ft_id', $ft_id)
                                    ->where('students.dp_id', $dp_id)
                                    ->select([
                                        'students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id'
                                    ])
                                    ->orderBy('students.student_id')
                                    ->get();
        $transferStudents = [];
        foreach($studentTransfer as $student){
            $count = UlangPay::where('st_id', $student->st_id)->where('up_year', $admissionRegisterYear)->exists();
            if($count == true){

            }else{
                $transferStudents[] = array(
                    'st_id' => $student->st_id,
                    'firstname_rumi' => $student->firstname_rumi,
                    'lastname_rumi' => $student->lastname_rumi,
                    'firstname_jawi' => $student->firstname_jawi,
                    'lastname_jawi' => $student->lastname_jawi,
                    'student_id' => $student->student_id
                );
            }
        }

        return [
            // 'request' => $request->all(),
            // 'currentAcademicYear' => $currentAcademicYear,
            // 'beforCurrentAcademicYear' => $beforCurrentAcademicYear,
            'studentCanExam' => $normalStudents,
            'studentTransfer' => $transferStudents,
        ];
    }

    //informasi dan status bayaran yuran
    public function yuranPaymentInfo(Request $request)
    {
        //student_register data
        $st_id = $request->st_id;
        $sr_id = $request->sr_id;
        $studentRegister = StudentRegister::where('sr_id', $sr_id)
                            ->where('st_id', $st_id)
                            ->first();

        //payStatus transforming
        switch($studentRegister->pay_status){
            case 'Belum bayar':
                $payStatus = 0;
                break;
            case 'BELUM LUNAS': 
                $payStatus = 1;
                break;
            case 'SUDAH LUNAS': 
                $payStatus = 2;
                break;
        }

        //payRemaining calculating
        $re_id = $studentRegister->re_id;
        $register = Register::where('re_id', $studentRegister->re_id)
                        ->first();
        $rs_type = $studentRegister['rs_type'];
        $registerPrize = $register->$rs_type;

        $payment = Payment::where('st_id', $st_id)->where('sr_id', $sr_id)->sum('money');
        $payRemaining = $registerPrize - $payment;

        //payment history
        $payments = Payment::where('st_id', $st_id)->where('sr_id', $sr_id)->orderBy('reciet_number', 'DESC')->get();

        return [
            'request' => $request->all(),
            'payStatus' => $payStatus,
            'payRemaining' => $payRemaining,
            'payPenalty' => 0,
            'payments' => $payments,
        ];
    }
}
