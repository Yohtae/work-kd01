<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');
        //authorization
        //$this->authorize('isAdmin');
    }

    public function index()
    {
        $users = User::latest()->paginate(10);
        return $users;   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'idcard' => $request['idcard'],
            'telephone' => $request['telephone'],
            'email' => $request['email'],
            'type' => $request['type'],
            'ft_id' => $request['ft_id'],
            'dp_id' => $request['dp_id'],
            'username' => $request['username'],
            'password' => Hash::make($request['password']),
        ]);
        // return ['status' => 'success'];
        return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request->password)]);
        }
        User::where('id',$id)->update($request->all());
        return ['status' => 'success'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function findUser(Request $request)
    {
        $query = $request->q;
        $users = User::where('name', 'like', '%'.$query.'%')
                ->orWhere('lastname', 'like', '%'.$query.'%')
                ->orWhere('telephone', 'like', '%'.$query.'%')
                ->orWhere('idcard', 'like', '%'.$query.'%')
                ->paginate(10);
        return $users; 
    }
}
