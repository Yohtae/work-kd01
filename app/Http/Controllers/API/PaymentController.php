<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Payment;
use App\StudentRegister;
use App\Register;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //sleep(1);
        $rules = [
            'money' => 'required|integer|min:1',
        ];

        $messages = [
            'money.required' => 'di perlukan',
            'money.integer' => 'jumlah duit salah',
            'money.min' => 'jumlah duit salah',
        ];

        $request->validate($rules, $messages);

        //cari nobor resit
        $studentRegister = StudentRegister::where('sr_id', $request->sr_id)->first();
        $academic_year = $studentRegister['academic_year'];
        $exists = Payment::where('academicYear', $academic_year)
                        ->orderBy('reciet_number')
                        ->exists();
        
        if($exists == false){
            $reciet_number = 1;
            $reciet_code = 'Y'.substr($academic_year,2).'/'.str_pad(1, 4, "0", STR_PAD_LEFT);   
        }else{
            $payments = Payment::where('academicYear', $academic_year)
                            ->orderBy('reciet_number')
                            ->get();
            $count = 1;
            foreach($payments as $payment){
                $number = $payment->reciet_number;
                if($number != $count){
                    $reciet_number = $count;
                    $reciet_code = 'Y'.substr($academic_year,2).'/'.str_pad($reciet_number, 4, "0", STR_PAD_LEFT);
                    break;
                }else{
                    $reciet_number = $number+1;
                    $reciet_code = 'Y'.substr($academic_year,2).'/'.str_pad($reciet_number, 4, "0", STR_PAD_LEFT);
                }
                $count++;
            }
        }

        //create payments
        $user_id = auth('api')->user()->id;
        $data = array(
            'sr_id' => $request->sr_id,
            'st_id' => $request->st_id,
            'user_id' => $user_id,
            'pay_date' => date('Y-m-d'),
            'money' => $request->money,
            'penalty' => 0,
            'reciet_code' => $reciet_code,
            'reciet_number' => $reciet_number,
            'academicYear' => $academic_year,
        );
        Payment::create($data);

        //update pay_status in studnt_register
        $register = Register::where('re_id', $studentRegister['re_id'])->first();
        $rs_type = $studentRegister['rs_type'];
        $registerPrize = $register->$rs_type;

        $sumPayment = Payment::where('st_id', $request->st_id)->where('sr_id', $request->sr_id)->sum('money');
        $payRemaining = $registerPrize - $sumPayment;

        if($sumPayment >= $registerPrize){
            $pay_status = 'SUDAH LUNAS';
            $payStatus = 2;
        }else{
            $pay_status = 'BELUM LUNAS';
            $payStatus = 1;
        }

        StudentRegister::where('sr_id', $request->sr_id)->update(['pay_status' => $pay_status]);

        //payment history
        $paymentsHistory = Payment::where('st_id', $request->st_id)
                                ->where('sr_id', $request->sr_id)
                                ->orderBy('reciet_number', 'DESC')
                                ->get();

        return [
            'status' => 'success',
            'payStatus' => $payStatus,
            'payRemaining' => $payRemaining,
            'payPenalty' => 0,
            'payments' => $paymentsHistory,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function yuranCancelPay(Request $request)
    {
        $rules = [
            'password' => 'required',
        ];

        $messages = [
            'password.required' => 'di perlukan',
        ];

        $request->validate($rules, $messages);

        $hashedPassword = auth('api')->user()->getAuthPassword();
        $password = $request->password;

        if(Hash::check($password, $hashedPassword)) {
            $status = 'match';
            Payment::where('p_id', $request->p_id)->delete();
        }else{
            $status = 'not';
        }

        //update pay_status in studnt_register
        $studentRegister = StudentRegister::where('sr_id', $request->sr_id)->first();
        $register = Register::where('re_id', $studentRegister['re_id'])->first();
        $rs_type = $studentRegister['rs_type'];
        $registerPrize = $register->$rs_type;
        $exists = Payment::where('sr_id', $request->sr_id) ->exists();

        $sumPayment = Payment::where('st_id', $request->st_id)->where('sr_id', $request->sr_id)->sum('money');
        $payRemaining = $registerPrize - $sumPayment;

        if($sumPayment >= $registerPrize){
            $pay_status = 'SUDAH LUNAS';
            $payStatus = 2;
        }else if($exists == false){
            $pay_status = 'Belum bayar';
            $payStatus = 0;
        }else{
            $pay_status = 'BELUM LUNAS';
            $payStatus = 1;
        }

        StudentRegister::where('sr_id', $request->sr_id)->update(['pay_status' => $pay_status]);

        //payment history
        $paymentsHistory = Payment::where('sr_id', $request->sr_id)
                            ->orderBy('reciet_number', 'DESC')
                            ->get();
        
        return [
            'status' => $status,
            'payStatus' => $payStatus,
            'payRemaining' => $payRemaining,
            'payPenalty' => 0,
            'payments' => $paymentsHistory,
            'exists' => $exists,
            'rs_type' => $rs_type,
        ];
    }
}
