<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdmissionPretest;
use App\AdmissionTestRoom;
use App\Http\Controllers\API\DistrictController;
use App\Http\Controllers\API\AmphurController;
use App\Http\Controllers\API\ProvinceController;
use App\Http\Controllers\API\FakultyController;
use App\Http\Controllers\API\DepartmentController;

class AdmissionPretestController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //daftar kursi semua mengikit atr_id
    public function seatList($atr_id)
    {
        $list = AdmissionPretest::where('atr_id',$atr_id)
                ->orderBy('seat_number')
                ->get();
        return $list;
    }
    public function seatCount($atr_id)
    {
        $count = AdmissionPretest::where('atr_id',$atr_id)->count();
        return $count;
    }
    public function findSeat($atr_id,$seat_number)
    {
        $count = AdmissionPretest::where('atr_id',$atr_id)
                                    ->where('seat_number',$seat_number)
                                    ->count();
        return $count;
    }
    //jumlah calon dalam kelas
    public function admissionExaminerCount($atr_id)
    {
        $count = AdmissionPretest::where('atr_id',$atr_id)->count();
        return $count;
    }
    //lis calon dalam kelas
    public function admissionPretestStudent($atr_id, FakultyController $faculty, DepartmentController $department)
    {
        $room = AdmissionTestRoom::where('id',$atr_id)->first('room_name')->room_name;
        $students = AdmissionPretest::join('students', 'students.st_id', '=', 'admission_pretests.st_id')
                                    ->join('pretest', 'pretest.st_id', '=', 'admission_pretests.st_id')
                                    ->where('admission_pretests.atr_id',$atr_id)
                                    ->select('admission_pretests.seat_number','students.cityzen_id',
                                            'students.sanawi_graduate','students.firstname_jawi',
                                            'students.lastname_jawi','pretest.odrNumber',
                                            'pretest.first_ftId','first_dpId','pretest.second_ftId',
                                            'pretest.second_dpId')
                                    ->orderBy('admission_pretests.seat_number')
                                    ->get();
        if($students != '[]'){
            foreach($students as $student){

                $firstFaculty = $faculty->show($student->first_ftId);
                $firstDepartment = $department->show($student->first_dpId);
                $secondFakulty = $faculty->show($student->second_ftId);
                $secondDepartment = $department->show($student->second_dpId);

                $value[] = array(
                    'بيل' => $student->seat_number,
                    'نمبر كاد ڤڠنالن' => $student->cityzen_id,
                    'نام' => $student->firstname_jawi,
                    'نسب' => $student->lastname_jawi,
                    'نمبر دفتر' => $student->odrNumber,
                    'لولوس ثنوي' => $student->sanawi_graduate,
                    'ڤيليهن ڤرتام' => $firstFaculty['ft_arab_name'].' '.$firstDepartment['dp_short_name'],
                    'ڤيليهن كدوا' => $secondFakulty['ft_arab_name'].' '.$secondDepartment['dp_short_name'],
                );
            }
        }else{
            $value = [];
        }
        
        return ['room' => $room, 'studentList' => $value];
    }
    
    //lis calon mahasiswa dalam kelas serta biodata
    public function admissionPretestBiodataStudent($atr_id,DistrictController $districtController,AmphurController $amphurController,ProvinceController $provinceController)
    {
        $room = AdmissionTestRoom::where('id',$atr_id)->first();
        $students = AdmissionPretest::join('students', 'students.st_id', '=', 'admission_pretests.st_id')
                                    ->join('pretest', 'pretest.st_id', '=', 'admission_pretests.st_id')
                                    ->where('admission_pretests.atr_id',$atr_id)
                                    ->select('admission_pretests.seat_number','students.cityzen_id',
                                            'students.firstname_jawi','students.lastname_jawi',
                                            'pretest.odrNumber','students.t_village_name',
                                            'students.house_number','students.place',
                                            'students.t_subdistrict','students.t_district',
                                            'students.post','students.t_province','students.father_name','students.father_lastname','students.sanawiVillage',
                                            'students.mother_name','students.mother_lastname','students.t_village_name','students.sanawi_graduate')
                                    ->orderBy('admission_pretests.seat_number')
                                    ->get();
        if($students != '[]'){
            foreach($students as $student){
                $t_village_name = $student->t_village_name;
                $house_number = $student->house_number;
                $place = $student->place;
                $t_subdistrict = $student->t_subdistrict;
                $t_district = $student->t_district;
                $t_province = $student->t_province;

                //alamat
                $district = $districtController->district($t_subdistrict)->DISTRICT_NAME;
                $amphur = $amphurController->amphur($t_district)->AMPHUR_NAME;
                $province = $provinceController->province($t_province)->PROVINCE_NAME;

                $value[] = array(
                    'بيل' => $student->seat_number,
                    //'نمبر كاد ڤڠنالن' => $student->cityzen_id,
                    'نام' => $student->firstname_jawi,
                    'نسب' => $student->lastname_jawi,
                    //'نمبر دفتر' => $student->odrNumber,
                    'نام نسب باڤ' => $student->father_name." ".$student->father_lastname,
                    'نام نسب ايبو' => $student->mother_name." " .$student->mother_lastname,
                    'لولوس ثنوي' => $student->sanawi_graduate." ".$student->sanawiVillage,
                    'كمڤوڠ' => $student->t_village_name,
                    'مقيم' => $district,
                    'دائرة' => $amphur,
                    'ولاية' => $province,
                    'ڤوس كود' => $student->post,
                );
            }
        }else{
            $value = [];
        }

        if($room->room_gender == 1){
            $room_gender = 'Lelaki';
        }else{
            $room_gender = 'Perempuan';
        }
        
        return [
            'room_gender' => $room_gender,
            'room_name' => $room->room_name, 
            'studentList' => $value,
        ];
    }

}
