<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Image;
use App\Pretest;
use App\Fakulty;
use App\Department;
use App\Province;
use App\Amphur;
use App\District;
use App\Http\Requests\StudentRequest;
use App\Imports\StudentCodeImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\API\AdmissionRegisterController;
use App\Http\Controllers\API\FakultyController;
use App\Http\Controllers\API\DepartmentController;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::where('student_id','!=','')->orderBy('student_id', 'DESC')->paginate(20);
        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::where('st_id',$id)->first();
        return $student;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, $id)
    {

        Student::where('st_id', $id)->update($request->all());
        return [
            'status' => 'success',
            //'request' => $request->all(),
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Mahasiswa yang daftar pada tahun di pilih
    // public function getStudentAtYear(){   
    //     $students = Student::where('class',$year)
    //                 ->orderBy('student_id')
    //                 ->get();
    //     return $students;
    // }

    //Mahasiswa yang daftar masuk mengikut tahun yang di pilih
    // public function studentAtYear()
    // {
    //     $students = Student::where('class',$year)
    //                 ->get();
    // }

    //Mahasiswa mengikut pegawai fakulti pengguna
    public function studentAtFaculty()
    {
        $user = auth('api')->user();
        $faculty = $user->ft_id;
        $department = $user->dp_id;
        $ft_id = $faculty;
        if($department==NULL){
            $dp_id = 0;
        }else{
            $dp_id = $department;
        }
        $students = Student::where('ft_id',$ft_id)
                            ->where('dp_id',$dp_id)
                            ->where('student_id','<>', '')
                            ->orderBy('student_id','DESC')
                            ->paginate(20);
        return $students;
    }
    //cari mahasiswa mengikut fakulti pengguna
    public function findStudentAtFaculty(Request $request)
    {
        $user = auth('api')->user();
        $faculty = $user->ft_id;
        $department = $user->dp_id;
        $ft_id = $faculty;
        if($department==NULL){
            $dp_id = 0;
        }else{
            $dp_id = $department;
        }
        $query = $request->q;
        $students = Student::where('ft_id',$ft_id)
            ->where('dp_id',$dp_id)
            ->where('student_id','LIKE', '%'.$query.'%')
            ->orWhere('firstname_rumi','LIKE', '%'.$query.'%')
            ->where('student_id','<>', '')
            ->orWhere('lastname_rumi','LIKE', '%'.$query.'%')
            ->where('student_id','<>', '')
            ->orWhere('firstname_jawi','LIKE', '%'.$query.'%')
            ->where('student_id','<>', '')
            ->orWhere('lastname_jawi','LIKE', '%'.$query.'%')
            ->where('student_id','<>', '')
            ->orderBy('student_id','DESC')
            ->paginate(20);
        return $students;
    }
    public function confirmingRegister(StudentRequest $request, $id)
    {
        return $id;
    }
    //import student_id
    public function importStudentCode(Request $request, AdmissionRegisterController $admissionRegisterController)
    {
        $rules = [
            'import_file' => 'required|file|mimes:xls,xlsx',
        ];

        $messages = [
            'import_file.required' => 'กรุณาเลือกไฟล์นามสกุล .xls หรือ xlsx',
            'import_file.file' => 'กรุณาเลือกไฟล์นามสกุล .xls หรือ xlsx',
            'import_file.mimes' => 'กรุณาเลือกไฟล์นามสกุล .xls หรือ .xlsx',
        ];

        $request->validate($rules, $messages);

        $path = $request->file('import_file');
        $data = Excel::toArray(new StudentCodeImport, $path);

        $ar_year = $admissionRegisterController->currentAdmissionRegister()->ar_year;

        foreach($data as $item){
            foreach($item as $i){
                $number = $i[0];//bilangan
                $student_code = $i[1];//NIM
                $idcard = $i[2];//No. identitas
                $name = $i[3];//nama
                $lastname = $i[4];//nasab

                //query std_id dari table students
                $student = Student::where('cityzen_id', $idcard)->first(['st_id']);
                $st_id = $student['st_id'];

                //query data std_photo dari table pretest
                $pretest = Pretest::where('st_id', $st_id)->first(['std_photo']);
                $std_photo = $pretest['std_photo'];

                Student::where('st_id', $st_id)->update([
                    'student_id' => $student_code,
                    'income_year' => $ar_year,
                    'password' => 'calon2020'
                ]);

                $img_exist = Image::where('st_id', $st_id)->exists();

                //update data students (student_id , income_year , password)
                if($std_photo == ''){

                }else if($img_exist == true){
                    Image::where('st_id', $st_id)->update([
                        'images' => $std_photo,
                        'name' => 'images/'.$std_photo,
                        'st_id' => $st_id,
                    ]);  
                }else{
                    //insert data image
                    Image::create([
                        'images' => $std_photo,
                        'name' => 'images/'.$std_photo,
                        'st_id' => $st_id,
                    ]);   
                }

                $value[] = array(
                    'number' => $number,
                    'idcard' => $idcard,
                    'student_code' => $student_code,
                    'name' => $name,
                    'lastname' => $lastname,
                    'std_photo' => $std_photo,
                    'st_id' => $st_id,
                );
            }
        }

        return response()->json([
            'message' => 'uploaded successfully',
            'ar_year' => $ar_year,
            'data' => $value,
        ], 200); 
    }
    //cari mahasiswa
    public function findStudent(Request $request)
    {
        $q = $request->q;
        $students = Student::where('student_id', '!=', null)
                        ->where('cityzen_id', 'LIKE', '%'.$q.'%')
                        ->orWhere('student_id', 'LIKE', '%'.$q.'%')
                        ->orWhere('lastname_rumi', 'LIKE', '%'.$q.'%')
                        ->orWhere('firstname_rumi', 'LIKE', '%'.$q.'%')
                        ->orWhere('lastname_jawi', 'LIKE', '%'.$q.'%')
                        ->orWhere('firstname_jawi', 'LIKE', '%'.$q.'%')
                        ->orderBy('student_id', 'DESC')
                        ->paginate(20);
        return response()->json($students);
    }
    //informasi mahasiswa
    public function studentInfo($id, FakultyController $facultyController, DepartmentController $departmentController)
    {
        //\sleep(3);
        $biodata = Student::where('st_id', $id)->first();
        if(Image::where('st_id', $biodata->st_id)->exists()){
            $image = Image::where('st_id', $biodata->st_id)->first();
        }else{
            $image = 'null';
        }
        $faculty = Fakulty::all();
        $department = Department::where('ft_id', $biodata['ft_id'])->get();
        $province = Province::all();
        $amphur = Amphur::where('PROVINCE_ID', $biodata['t_province'])->get();
        $district = District::where('AMPHUR_ID', $biodata['t_district'])->get();
        $stdFaculty = $facultyController->show($biodata->ft_id);
        $stdDepartment = $departmentController->show($biodata->dp_id);

        return [
            'biodata' => $biodata,
            'studentFaculty' => $stdFaculty,
            'studentDepartment' => $stdDepartment,
            'image' => $image,
            'faculty' => $faculty,
            'department' => $department,
            'province' => $province,
            'amphur' => $amphur,
            'district' => $district,
        ];
    }
    //update data mahasiswa (biodata,gambar)
    public function updateStudentInfo(Request $request, $id)
    {
        $rules = [
            'student_id' => 'required|digits:8|unique:students,student_id,'.$id.',st_id',
            'cityzen_id' => 'required|digits:13|unique:students,cityzen_id,'.$id.',st_id',
            't_province' => 'required',
            't_district' => 'required',
            't_subdistrict' => 'required',
        ];

        $messages = [
            'student_id.required' => 'di perlukan',
            'student_id.digits' => 'harap sempurnakan NIM',
            'student_id.unique' => 'NIM ini sudah di guna', 
            'cityzen_id.required' => 'di perlukan',
            'cityzen_id.digits' => 'harap sempurnakan no identitas',
            'cityzen_id.unique' => 'no identitas ini sudah ada',
            't_province.required' => 'di perlukan',
            't_district.required' => 'di perlukan',
            't_subdistrict.required' => 'di perlukan',
        ];

        $request->validate($rules, $messages);

        Student::where('st_id', $id)->update($request->all());

        return [
            'status' => 'success',
            'data' => $request->all(),
        ];
    }
    //student take photo
    public function studentTakePhoto(Request $request)
    {
        if($request->std_photo != ''){
            $photo = '2020-'.time().'.' . explode('/', explode(':', substr($request->std_photo, 0, strpos($request->std_photo, ';')))[1])[1];
            //\Image::make($request->std_photo)->resize(370, 350)->save(public_path('storage/student_image/').$photo);
            \Image::make($request->std_photo)->crop(330, 350)->save(('./images/').$photo);
            //\Image::make($request->std_photo)->crop(400, 430)->save(('./images/').$photo);
            //\Image::make($request->std_photo)->crop(450, 460)->save(('./images/').$photo);
            //\Image::make($request->std_photo)->resize(300, 300)->save(('./images/').$photo);
            //$request->merge(['std_photo' => $photo ]);
        }

        Student::where('st_id', $request->st_id)->update(['photo' => $photo]);

        if(Image::where('st_id',$request->st_id)->exists() == true){
            Image::where('st_id', $request->st_id)->update(['images' => $photo, 'name' => 'images/'.$photo]);
        }else{
            Image::create(['images' => $photo, 'name' => 'images/'.$photo, 'st_id' => $request->st_id]);
        }

        return [
            // 'std_photo' => $photo,
            // 'data' => $request->all(),
            'status' => 'success',
        ];
    }
}
