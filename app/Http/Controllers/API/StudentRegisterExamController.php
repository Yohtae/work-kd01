<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentRegisterExam;

class StudentRegisterExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //mahasiswa yang dapat periksa pada semester & tahun yang di cari
    public function studentCanExam($semester, $year, $ft_id, $dp_id, $class)
    {
        $students = StudentRegisterExam::join('student_register', 'student_register_exam.st_id', '=', 'student_register.st_id')
                                        ->join('students', 'student_register_exam.st_id', '=', 'students.st_id')
                                        ->where('student_register.pay_status', 'SUDAH LUNAS')
                                        ->where('student_register_exam.pay_status', 'SUDAH LUNAS')
                                        ->where('student_register_exam.year', $year)
                                        ->where('student_register_exam.term', $semester)
                                        ->where('student_register.academic_year', $year)
                                        ->where('student_register.term', $semester)
                                        ->where('students.ft_id', $ft_id)
                                        ->where('students.dp_id', $dp_id)
                                        ->where('students.class', $class)
                                        ->select([
                                            'students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id'
                                        ])
                                        ->get();
        return $students;
    }
}
