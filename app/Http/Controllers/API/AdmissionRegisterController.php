<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdmissionRegister;
use App\User;
use App\Http\Requests\AdmissionRegisterRequest;

class AdmissionRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        //authorization
        //$this->authorize('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AdmissionRegister::join('users', 'users.id', '=', 'admission_registers.user_id')
                                ->select('admission_registers.id',
                                        'admission_registers.ar_year',
                                        'admission_registers.ar_start_date',
                                        'admission_registers.ar_end_date',
                                        'admission_registers.status',
                                        'admission_registers.ar_register_cost',
                                        'admission_registers.ar_muqaddimah_cost',
                                        'users.name',
                                        'users.lastname')
                                ->orderBy('ar_year','DESC')
                                ->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdmissionRegisterRequest $request)
    {
        AdmissionRegister::where('status',1)->update(['status' => 2]);
        $user_id = auth('api')->user()->id;
        $request->merge(['user_id' => $user_id]);
        AdmissionRegister::create($request->all());
        return ['status' => $request->all()];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdmissionRegisterRequest $request, $id)
    {
        $user_id = auth('api')->user()->id;
        $request->merge(['user_id' => $user_id]);
        AdmissionRegister::query()->update(['status' => 2]);
        AdmissionRegister::where('id', $id)
                        ->update([
                            'ar_year' => $request->ar_year,
                            'ar_start_date' => $request->ar_start_date,
                            'ar_end_date' => $request->ar_end_date,
                            'status' => $request->status,
                            'ar_register_cost' => $request->ar_register_cost,
                            'ar_muqaddimah_cost' => $request->ar_muqaddimah_cost
                        ]);
        return [
            'status' => 'success',
            //'data' => $id,
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function currentAdmissionRegister()
    {
        $admissionRegister = AdmissionRegister::where('status',1)->first(['id','ar_year','ar_muqaddimah_cost']);
        return $admissionRegister;
    }
}
