<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\studentRegisterUpdateRequest;
use App\StudentSubject;
use App\Student;
use App\Http\Controllers\API\StudentRegisterController;
use App\Register;
use App\StudentRegister;

class StudentSubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updateStudentSubject(StudentRegisterController $StudentRegisterController, StudentRegisterUpdateRequest $request)
    {
        $students =  $StudentRegisterController->findStudentRegister($request);
        foreach($students as $student){
            $st_id = $student->student->st_id;
            $register = Register::where('re_id',$request->re_id)->first();
            $ss_term = $register->re_term;
            $ss_year = $register->re_year;
            StudentSubject::where('st_id',$st_id)
                            ->where('ss_term',$ss_term)
                            ->where('ss_year',$ss_year)
                            ->update(array('ss_class' => $request->ss_class));
        }
        return ['statuss' => 'success'];
    }
    public function studentSubject($st_id)
    {
        $studentInfo = Student::where('st_id',$st_id)->first();
        $studentRegister = StudentRegister::where('st_id',$st_id)
                            ->orderBy('academic_year')
                            ->orderBy('term')
                            ->get();
        foreach($studentRegister as $register){
            $year = $register->academic_year;
            $term = $register->term;
            $subjects = StudentSubject::with('subject')
                        ->with('teacher')
                        ->where('st_id',$st_id)
                        ->where('ss_term',$term)
                        ->where('ss_year',$year)
                        ->get();
            $subjectsData[] = array(
                'student_id' => $studentInfo['student_id'],
                'studentNameRumi' => $studentInfo['firstname_rumi'],
                'studentLastnameRumi' => $studentInfo['lastname_rumi'],
                'studentNameJawi' => $studentInfo['firstname_jawi'],
                'studentLastnameJawi' => $studentInfo['lastname_jawi'],
                'semester' => $term,
                'year' => $year,
                'subject' => $subjects
            );
        }
        return $subjectsData;
    }
    public function updateStudentScore($st_id, Request $request)
    {
        foreach($request->score as $semester){
            $term = $semester['semester'];
            $year = $semester['year'];
            foreach($semester['subject'] as $subject){
                StudentSubject::where('ss_id',$subject['ss_id'])
                ->update(array('ss_score' => $subject['ss_score']));
            }
        }
        return ['status' => 'success'];
    }
}
