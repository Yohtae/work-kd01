<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AdmissionRegister;
use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\FakultyController;
use App\Http\Controllers\API\DepartmentController;
use App\MuqaddimahPay;
use App\Student;
use App\Pretest;
use App\Fakulty;
use App\Department;
use App\UlangPay;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    //laporan mahasiswa bayar muqaddimah semua fakulti
    public function muqaddimahReportStatistic(AdmissionRegisterController $AdmissionRegisterController)
    {
        $pre_register_year = $AdmissionRegisterController->currentAdmissionRegister()->ar_year;
        $faculties = Fakulty::all();
        foreach($faculties as $faculty){
            $ft_id = $faculty->ft_id;
            $departmentCount = Department::where('ft_id', $ft_id)->count();
            if($departmentCount==0){
                //Mahasiswa tahun satu
                //lelaki
                $maleClassOne = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 0)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //perempuan
                $femaleClassOne = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 0)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Perempuan');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //mahasiswa transfer
                //lelaki
                $maleTransfer = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 1)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //perempuan
                $femaleTransfer = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 1)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Perempuan');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                $classOne[] = array(
                    'faculty' => $faculty->ft_name,
                    'department' => '',
                    'male' => $maleClassOne,
                    'female' => $femaleClassOne,
                    'sumStudent' => $maleClassOne+$femaleClassOne,
                );
                $transfer[] = array(
                    'faculty' => $faculty->ft_name,
                    'department' => '',
                    'male' => $maleTransfer,
                    'female' => $femaleTransfer,
                    'sumStudent' => $maleTransfer+$femaleTransfer,
                );
            }else{
                $departments = Department::where('ft_id', $faculty->ft_id)->get();
                foreach($departments as $department){
                    $dp_id = $department->dp_id;
                    //Mahasiswa tahun satu
                    //lelaki
                    $maleClassOne = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 0)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id, $dp_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                            $q->where('dp_id', $dp_id);
                        })
                        ->count();
                    //perempuan
                    $femaleClassOne = Pretest::where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('type', 0)
                            ->with('student')
                            ->with('muqaddimahPay')
                            ->whereHas('student', function($q) use ($ft_id, $dp_id){
                                $q->where('gender','Perempuan');
                                $q->where('ft_id', $ft_id);
                                $q->where('dp_id', $dp_id);
                            })
                            ->count();
                    //mahasiswa transfer
                    //lelaki
                    $maleTransfer = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 1)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id, $dp_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                            $q->where('dp_id', $dp_id);
                        })
                        ->count();
                    //perempuan
                    $femaleTransfer = Pretest::where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('type', 1)
                            ->with('student')
                            ->with('muqaddimahPay')
                            ->whereHas('student', function($q) use ($ft_id, $dp_id){
                                $q->where('gender','Perempuan');
                                $q->where('ft_id', $ft_id);
                                $q->where('dp_id', $dp_id);
                            })
                            ->count();
                    $classOne[] = array(
                        'faculty' => $faculty->ft_name,
                        'department' => $department->dp_short_name,
                        'male' => $maleClassOne,
                        'female' => $femaleClassOne,
                        'sumStudent' => $maleClassOne+$femaleClassOne,
                    );
                    $transfer[] = array(
                        'faculty' => $faculty->ft_name,
                        'department' => $department->dp_short_name,
                        'male' => $maleTransfer,
                        'female' => $femaleTransfer,
                        'sumStudent' => $maleTransfer+$femaleTransfer,
                    );
                }
            }
        }

        $totalClassOne = Pretest::where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('type', 0)
                            ->with('muqaddimahPay')
                            ->whereHas('muqaddimahPay', function($q) use ($pre_register_year){
                                $q->where('m_academicyear', $pre_register_year);
                            })
                            ->count();
        $totalTransfer = Pretest::where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('type', 1)
                            ->with('muqaddimahPay')
                            ->whereHas('muqaddimahPay', function($q) use ($pre_register_year){
                                $q->where('m_academicyear', $pre_register_year);
                            })
                            ->count();

        return [
            'classOneStatistic' => $classOne,
            'transferStatistic' => $transfer,
            'totalClassOne' => $totalClassOne,
            'totalTransfer' => $totalTransfer,
        ];
    }

    //laporan duit muqaddimah
    public function muqaddimahReportMoney(Request $request, AdmissionRegisterController $AdmissionRegisterController)
    {
        $type = $request->type;
        $pre_register_year = $AdmissionRegisterController->currentAdmissionRegister()->ar_year;
        $today = date('Y-m-d');
        $payed = Pretest::join('students', 'students.st_id', '=', 'pretest.st_id')
                ->join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                ->where('pretest.payStatus',1)
                ->where('pretest.type', $type)
                ->where('muqaddimah_pay.m_academicyear', $pre_register_year)
                ->where('muqaddimah_pay.m_paydate', $today)
                ->orderBy('muqaddimah_pay.m_number', 'ASC')
                ->select([
                        'students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi',
                        'muqaddimah_pay.m_number','muqaddimah_pay.m_reciet','muqaddimah_pay.updated_at','muqaddimah_pay.m_paydate'
                        ])
                ->get();
        $money = Pretest::join('students', 'students.st_id', '=', 'pretest.st_id')
                ->join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                ->where('pretest.payStatus',1)
                ->where('pretest.type', $type)
                ->where('muqaddimah_pay.m_academicyear', $pre_register_year)
                ->where('muqaddimah_pay.m_paydate', $today)
                ->orderBy('muqaddimah_pay.m_number', 'ASC')
                ->select([
                        'students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi',
                        'muqaddimah_pay.m_number','muqaddimah_pay.m_reciet','muqaddimah_pay.updated_at','muqaddimah_pay.m_paydate'
                        ])
                ->sum('m_money');
        $totalMoney = Pretest::join('students', 'students.st_id', '=', 'pretest.st_id')
                ->join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                ->where('pretest.payStatus',1)
                ->where('pretest.type', $type)
                ->where('muqaddimah_pay.m_academicyear', $pre_register_year)
                ->orderBy('muqaddimah_pay.m_number', 'ASC')
                ->select([
                        'students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi',
                        'muqaddimah_pay.m_number','muqaddimah_pay.m_reciet','muqaddimah_pay.updated_at','muqaddimah_pay.m_paydate'
                        ])
                ->sum('m_money');

        return [
            'type' => $type,
            'list' => $payed, 
            'money' => number_format($money),
            'totalMoney' => number_format($totalMoney),
        ];
    }
    public function findmuqaddimahReportMoneyDateRange(Request $request, AdmissionRegisterController $AdmissionRegisterController)
    {
        $pre_register_year = $AdmissionRegisterController->currentAdmissionRegister()->ar_year;
        $payed = Pretest::join('students', 'students.st_id', '=', 'pretest.st_id')
                ->join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                ->where('pretest.payStatus',1)
                ->where('pretest.type', $request->type)
                ->where('muqaddimah_pay.m_academicyear', $pre_register_year)
                ->whereBetween('muqaddimah_pay.m_paydate', array($request->fromDate, $request->toDate))
                ->orderBy('muqaddimah_pay.m_number', 'ASC')
                ->select([
                        'students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi',
                        'muqaddimah_pay.m_number','muqaddimah_pay.m_reciet','muqaddimah_pay.updated_at','muqaddimah_pay.m_paydate'
                        ])
                ->get();
        $totalMoney = Pretest::join('students', 'students.st_id', '=', 'pretest.st_id')
                ->join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                ->where('pretest.payStatus',1)
                ->where('pretest.type', $request->type)
                ->where('muqaddimah_pay.m_academicyear', $pre_register_year)
                ->whereBetween('muqaddimah_pay.m_paydate', array($request->fromDate, $request->toDate))
                ->orderBy('muqaddimah_pay.m_number', 'ASC')
                ->select([
                        'students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi',
                        'muqaddimah_pay.m_number','muqaddimah_pay.m_reciet','muqaddimah_pay.updated_at','muqaddimah_pay.m_paydate'
                        ])
                ->sum('m_money');
        return [
            'list' => $payed,
            'totalMoney' => number_format($totalMoney),
        ];
    }
    public function currentClassList(RegisterController $registerController)
    {
        $class = $registerController->studentClass();
        return $class;
    }
    public function studentClassroom(Request $request, FakultyController $faculty, DepartmentController $department, RegisterController $register)
    {
        $class = $request->class;
        $year = $request->year;
        $ft_id = $request->ft_id;
        $dp_id = $request->dp_id;

        $currentAcademicYear = $register->currentAcademicYear()->re_year;

        //check kelas mahasiswa
        //if class is 1
        if($year == $currentAcademicYear){
            $students = Student::join('pretest', 'students.st_id', '=', 'pretest.st_id')
                            ->join('muqaddimah_pay', 'students.st_id', '=', 'muqaddimah_pay.st_id')
                            ->where('students.ft_id', $ft_id)
                            ->where('students.dp_id', $dp_id)
                            ->where('students.class', $year)
                            ->select([
                                'students.student_id','students.firstname_jawi','students.lastname_jawi',
                                'students.firstname_rumi','students.lastname_rumi','students.telephone',
                                ])
                            ->orderBy('students.gender')
                            ->orderBy('students.student_id')
                            ->groupBy('students.st_id')
                            ->get();
        }else{//if class is 2-4
            $students = Student::join('pretest', 'students.st_id', '=', 'pretest.st_id')
                            ->join('muqaddimah_pay', 'students.st_id', '=', 'muqaddimah_pay.st_id')
                            ->join('ulang_pays', 'students.st_id', '=', 'ulang_pays.st_id')
                            ->where('students.ft_id', $ft_id)
                            ->where('students.dp_id', $dp_id)
                            ->where('students.class', $year)
                            ->where('ulang_pays.up_year', $currentAcademicYear)
                            ->select([
                                'students.student_id','students.firstname_jawi','students.lastname_jawi',
                                'students.firstname_rumi','students.lastname_rumi','students.telephone',
                                ])
                                ->orderBy('students.gender')
                                ->orderBy('students.student_id')
                                ->groupBy('students.st_id')
                            ->get();
        }

        return [
            'status' => 'success',
            'year' => $year,
            'class' => $class,
            'faculty' => $faculty->show($ft_id)->ft_arab_name,
            'department' => $department->show($dp_id)->dp_arab_name,
            'ft_name' => $faculty->show($ft_id)->ft_name,
            'dp_name' => $department->show($dp_id)->dp_name,
            'dp_short_name' => $department->show($dp_id)->dp_short_name, 
            'students' => $students,
            'print_date' => date('Y-m-d'),
        ];
    }
    public function studentClassroomExell(Request $request, FakultyController $faculty, DepartmentController $department, RegisterController $register)
    {
        $class = $request->class;
        $year = $request->year;
        $ft_id = $request->ft_id;
        $dp_id = $request->dp_id;

        $currentAcademicYear = $register->currentAcademicYear()->re_year;

        //check kelas mahasiswa
        //if class is 1
        if($year == $currentAcademicYear){
            $query = Student::join('pretest', 'students.st_id', '=', 'pretest.st_id')
                            ->join('muqaddimah_pay', 'students.st_id', '=', 'muqaddimah_pay.st_id')
                            ->where('ft_id', $ft_id)
                            ->where('dp_id', $dp_id)
                            ->where('class', $year)
                            ->select([
                                'student_id','firstname_jawi','lastname_jawi',
                                'firstname_rumi','lastname_rumi','telephone','cityzen_id'
                                ])
                                ->orderBy('gender')
                                ->orderBy('student_id')
                                ->groupBy('students.st_id')
                            ->get();
        }else{//if class is 2-4
            $query = Student::join('pretest', 'students.st_id', '=', 'pretest.st_id')
                            ->join('muqaddimah_pay', 'students.st_id', '=', 'muqaddimah_pay.st_id')
                            ->join('ulang_pays', 'students.st_id', '=', 'ulang_pays.st_id')
                            ->where('students.ft_id', $ft_id)
                            ->where('students.dp_id', $dp_id)
                            ->where('students.class', $year)
                            ->where('ulang_pays.up_year', $currentAcademicYear)
                            ->select([
                                'student_id','firstname_jawi','lastname_jawi',
                                'firstname_rumi','lastname_rumi','telephone','cityzen_id'
                                ])
                                ->orderBy('gender')
                                ->orderBy('student_id')
                                ->groupBy('students.st_id')
                            ->get();
        }

        // $query = Student::join('pretest', 'students.st_id', '=', 'pretest.st_id')
        //                 ->join('muqaddimah_pay', 'students.st_id', '=', 'muqaddimah_pay.st_id')
        //                 ->where('ft_id', $ft_id)
        //                 ->where('dp_id', $dp_id)
        //                 ->where('class', $year)
        //                 ->select([
        //                     'student_id','firstname_jawi','lastname_jawi','cityzen_id',
        //                     'firstname_rumi','lastname_rumi','telephone',
        //                     ])
        //                     ->orderBy('gender')
        //                     ->orderBy('student_id')
        //                 ->get();
        if($query != '[]'){
            $i = 1;
            foreach($query as $student){
                $students[] = array(
                    'بيل' => $i,
                    'نمبرڤوكؤ' => $student->student_id,
                    'نمبر كاد ڤڠنالن' => $student->cityzen_id,
                    'نام' => $student->firstname_jawi,
                    'نسب' => $student->lastname_jawi,
                );
                $i++;
            }
        }else{
            $students = [];
        }
        return [
            'class' => $class,
            'ft_name' => $faculty->show($ft_id)->ft_name,
            'dp_name' => $department->show($dp_id)->dp_name,
            'dp_short_name' => $department->show($dp_id)->dp_short_name, 
            'students' => $students,
        ];
    }
}
