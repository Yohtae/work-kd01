<?php

namespace App\Http\Controllers\API;

use App\Exports\AdmissionPretestExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pretest;
//use App\Http\Requests\PretestRequest;
//use App\AdmissionRegister;
use App\Http\Controllers\API\AdmissionRegisterController;
use App\Http\Controllers\API\AdmissionTestRoomController;
use App\Http\Controllers\API\AdmissionPretestController;
use App\Http\Controllers\API\FakultyController;
use App\Http\Controllers\API\DepartmentController;
use App\AdmissionPretest;
use App\AdmissionTestRoom;
use Storage;
use App\Student;
use App\Fakulty;
use App\Department;
use App\Province;
use App\District;
use App\Amphur;
use App\MuqaddimahPay;

class PretestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        //authorization
        //$this->authorize('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pretest = Pretest::with('student')
                    ->where('pre_register_year','2020')
                    ->latest()
                    ->paginate(10);
        return $pretest;   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pretest = Pretest::where('pre_id',$id)->first();
        return $pretest;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function confirmingRegister(Request $request, $id, AdmissionRegisterController $AdmissionRegisterController, AdmissionTestRoomController $AdmissionTestRoomController, AdmissionPretestController $admissionPretestController)
    {
        if($request->first_ftId == '121'){
            $first_dpId = 'required';
        }else{
            $first_dpId = '';
        }

        if($request->second_ftId == '125'){
            $second_dpId = 'required';
        }else{
            $second_dpId = '';
        }
        $validate = $request->validate(
            [
                'first_ftId' => 'required',
                'first_dpId' => $first_dpId,
                'second_ftId' => 'required',
                'second_dpId' => $second_dpId,
            ]
        );

        //cari admission_register yang status=1
        $admissionRegister = $AdmissionRegisterController->currentAdmissionRegister();
        //bilik periksa tahun yang buka daftar sekarang
        $testRooms = $AdmissionTestRoomController->testRoomByGender($request->gender);

        foreach($testRooms as $testRoom){
            $count = AdmissionPretest::where('atr_id',$testRoom->id)->count();

            if($count<30){

                //check nombor kursi
                $admPretests = $admissionPretestController->seatList($testRoom->id);
                $admCount = $admissionPretestController->seatCount($testRoom->id);
                if($admCount == 0){
                    $seat = 1;
                }else{
                    for($i=1;$i<=30;$i++){
                        $findSeat = $admissionPretestController->findSeat($testRoom->id,$i);
                        if($findSeat == 0){
                            $seat = $i;
                            break;
                        }
                    }
                }

                $atr_id = $testRoom->id;
                $room_name = $testRoom->room_name;
                $seatNumber = $seat;
                break;
            }  
        }

        //cari data mahasiswa dalam table admission_pretests
        $studentInTestRoom = AdmissionPretest::where('st_id',$request->st_id)->count();
        if($studentInTestRoom>0){

        }else{
            //ord_number 
            $ordNumber = Pretest::where('pre_register_year',$admissionRegister->ar_year)
                            ->where('payStatus',1)
                            ->count();
            AdmissionPretest::create([
                'atr_id' => $atr_id,
                'st_id' => $request->st_id,
                'seat_number' => $seatNumber,
            ]);
            Pretest::where('st_id',$request->st_id)
                    ->update(
                        array(
                        'testClass' => $room_name,
                        'testNumber' => $seatNumber,
                        'odrNumber' => $ordNumber+1,
                        )
                    );
        }
        Pretest::where('st_id',$request->st_id)
            ->update(
                array(
                'first_ftId' => $validate['first_ftId'],
                'first_dpId' => $validate['first_dpId'],
                'second_ftId' => $validate['second_ftId'],
                'second_dpId' => $validate['second_dpId'],
                'payStatus' => 1
                )
            );

        return [
            'gender' => $request->gender,
            'admissionRegister' => $admissionRegister,
            'testRooms' => $testRooms,
            'atr_id' => $atr_id,
            'seatNumber' => $seatNumber,
            'studentTestRoom' => $studentInTestRoom,
        ];
    }
    public function cancelPretest(Request $request, $st_id)
    {
        $pp = AdmissionPretest::where('st_id',$st_id)->delete();
        Pretest::where('st_id',$st_id)->update(
                    array(
                        'payStatus' => 0,
                        'testClass' => NULL,
                        'testNumber' => NULL,
                        'odrNumber' => NULL,
                    )
                );
        return $st_id;
    }
    public function takePhoto(Request $request)
    {
        $currentPhoto = Pretest::where('st_id',$request->st_id)->first('std_photo');
        if($request->std_photo != $currentPhoto->std_photo){
            $photo = time().'.' . explode('/', explode(':', substr($request->std_photo, 0, strpos($request->std_photo, ';')))[1])[1];
            // \Image::make($request->std_photo)->resize(370, 350)->save(public_path('storage/student_image/').$photo);
            //\Image::make($request->std_photo)->resize(400, 300)->save(('./images/').$photo);
            \Image::make($request->std_photo)->crop(450, 460)->save(('./images/').$photo);
            //\Image::make($request->std_photo)->resize(300, 300)->save(('./images/').$photo);
            $request->merge(['std_pgoto' => $photo ]);
        }
        Pretest::where('st_id',$request->st_id)->update(array('std_photo' => $photo));
        return $currentPhoto->std_photo;
    }
    public function findPretest(Request $request, AdmissionRegisterController $AdmissionRegisterController)
    {
        $ar_year = $AdmissionRegisterController->currentAdmissionRegister()->ar_year;
        $query = $request->q;
        $students = Pretest::with('student')
                ->where('regNumber', '=', $query)
                ->where('pre_register_year',$ar_year)
                ->orWhereHas('student', function($q) use ($query){
                    $q->where('cityzen_id', 'LIKE', '%'.$query.'%');
                    $q->orWhere('firstname_rumi', 'LIKE', '%'.$query.'%');
                    $q->orWhere('lastname_rumi', 'LIKE', '%'.$query.'%');
                    $q->orWhere('t_studentname', 'LIKE', '%'.$query.'%');
                    $q->orWhere('t_studentlastname', 'LIKE', '%'.$query.'%');
                    $q->orWhere('firstname_jawi', 'LIKE', '%'.$query.'%');
                    $q->orWhere('lastname_jawi', 'LIKE', '%'.$query.'%');
                })
                ->where('pre_register_year',$ar_year)
                ->latest()
                ->paginate(10);
        return $students; 
    }
    public function admissionFormInfo($st_id)
    {
        $student = Student::where('st_id',$st_id)->first(['t_province','t_district','t_subdistrict']);
        $province = Province::where('PROVINCE_ID',$student->t_province)->first('PROVINCE_NAME');
        $amphur = Amphur::where('AMPHUR_ID',$student->t_district)->first('AMPHUR_NAME');
        $district = District::where('DISTRICT_ID',$student->t_subdistrict)->first('DISTRICT_NAME');
        $pretest = Pretest::where('st_id',$st_id)->first(['first_ftId','first_dpId','second_ftId','second_dpId']);
        $firstFaculty = Fakulty::where('ft_id',$pretest->first_ftId)->first(['ft_arab_name']);
        if($pretest->first_dpId == NULL){
            $firstDepartment = '';
        }else{
            $firstDepartment = Department::where('dp_id',$pretest->first_dpId)->first(['dp_short_name']);
            $firstDepartment = $firstDepartment['dp_short_name'];
        }
        $secondFaculty = Fakulty::where('ft_id',$pretest->second_ftId)->first(['ft_arab_name']);
        if($pretest->second_dpId == NULL){
            $secondDepartment = '';
        }else{
            $secondDepartment = Department::where('dp_id',$pretest->second_dpId)->first(['dp_short_name']);
            $secondDepartment = $secondDepartment['dp_short_name'];
        }
        return [ 
            'province' => $province->PROVINCE_NAME,
            'amphur' => $amphur->AMPHUR_NAME,
            'district' => $district->DISTRICT_NAME,
            'firstSelect' => $firstDepartment." ڤرتام : ".$firstFaculty->ft_arab_name,
            //'firstDepartment' => $firstDepartment,
            'secondSelect' => $secondDepartment." كدوا : ".$secondFaculty->ft_arab_name,
            //'secondDepartment' => $secondDepartment,
        ];
    }
    public function countRegister(AdmissionRegisterController $AdmissionRegisterController)
    {
        $pre_register_year = $AdmissionRegisterController->currentAdmissionRegister()->ar_year;
        $totalRegister = Pretest::where('pre_register_year',$pre_register_year)->count();
        $totalConfirm = Pretest::where('pre_register_year',$pre_register_year)->where('payStatus',1)->count();
        $totalTransfer = Pretest::where('pre_register_year',$pre_register_year)->where('type',2)->count();
        $totalMuqaddimah = Pretest::where('pre_register_year',$pre_register_year)->where('muqaddimah_pay_status',1)->count();
        return [
            'totalRegister' => $totalRegister,
            'totalConfirm' => $totalConfirm,
            'totalTransfer' => $totalTransfer,
            'totalMuqaddimah' => $totalMuqaddimah,
        ];
    }
    public function admissionRegisterStatistic(AdmissionRegisterController $AdmissionRegisterController)
    {
        $pre_register_year = $AdmissionRegisterController->currentAdmissionRegister()->ar_year;
        $facultys = Fakulty::all();
        foreach($facultys as $index => $faculty){
            $ft_id = $faculty->ft_id;
            $departmentCount = Department::where('ft_id',$faculty->ft_id)->count();
            $facultyName = Fakulty::where('ft_id',$faculty->ft_id)->first(['ft_name'])->ft_name;
            if($departmentCount==0){

                //total calon pilihan pertama & muqaddimah
                //calon lelaki
                $maleSelectFirst = Pretest::with('student')
                        ->where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('first_ftId',$faculty->ft_id)
                        ->whereHas('student', function($q){
                            $q->where('gender','Lelaki');
                        })
                        ->count();
                //muqaddimah lelaki
                $maleMuqaddimah = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 0)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //muqaddimah perempuan
                $femaleMuqaddimah = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 0)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Perempuan');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //muqaddimah transfer lelaki
                $maleMuqaddimahTransfer = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 1)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //muqaddimah transfer perempuan
                $femaleMuqaddimahTransfer = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 1)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id){
                            $q->where('gender','Perempuan');
                            $q->where('ft_id', $ft_id);
                        })
                        ->count();
                //perempuan
                $femaleSelectFirst = Pretest::with('student')
                        ->where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('first_ftId',$faculty->ft_id)
                        ->whereHas('student', function($q){
                            $q->where('gender','Perempuan');
                        })
                        ->count();

                //total calon pilihan kedua
                //lelaki
                $maleSelectSecond = Pretest::with('student')
                        ->where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('second_ftId',$faculty->ft_id)
                        ->whereHas('student', function($q){
                            $q->where('gender','Lelaki');
                        })
                        ->count();
                //perempuant
                $femaleSelectSecond = Pretest::with('student')
                        ->where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('second_ftId',$faculty->ft_id)
                        ->whereHas('student', function($q){
                            $q->where('gender','Perempuan');
                        })
                        ->count();
                $statisticFirst[] = array(
                                'faculty' => $facultyName,//$faculty->ft_id,
                                'department' => '',
                                'maleSelectFirst' => $maleSelectFirst,
                                'femaleSelectFirst' => $femaleSelectFirst,
                                'sumSelectFirst' => $maleSelectFirst+$femaleSelectFirst,
                            ); 
                $statisticSecond[] = array(
                                'faculty' => $facultyName,//$faculty->ft_id,
                                'department' => '',
                                'maleSelectSecond' => $maleSelectSecond,
                                'femaleSelectSecond' => $femaleSelectSecond,
                                'sumSelectSecond' => $maleSelectSecond+$femaleSelectSecond
                            ); 
                $muqaddimahPayed[] = array(
                    'faculty' => $facultyName,
                    'department' => '',
                    'male' => $maleMuqaddimah,
                    'female' => $femaleMuqaddimah,
                    'sumMuqaddimah' => $maleMuqaddimah+$femaleMuqaddimah,
                );
                $muqaddimahTransferPayed[] = array(
                    'faculty' => $facultyName,
                    'department' => '',
                    'male' => $maleMuqaddimah,
                    'female' => $femaleMuqaddimah,
                    'sumMuqaddimahTransfer' => $maleMuqaddimahTransfer+$femaleMuqaddimahTransfer,
                );
            }else{
                $departments = Department::where('ft_id',$faculty->ft_id)->get();
                $facultyName = Fakulty::where('ft_id',$faculty->ft_id)->first(['ft_name'])->ft_name;
                foreach($departments as $department){
                    $dp_id = $department->dp_id;
                    //total calon pilihan pertama & muqaddimah
                    //lelaki
                    $maleSelectFirst = Pretest::with('student')
                            ->where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('first_ftId',$faculty->ft_id)
                            ->where('first_dpId',$department->dp_id)
                            ->whereHas('student', function($q){
                                $q->where('gender','Lelaki');
                            })
                            ->count();
                    //perempuan
                    $femaleSelectFirst = Pretest::with('student')
                            ->where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('first_ftId',$faculty->ft_id)
                            ->where('first_dpId',$department->dp_id)
                            ->whereHas('student', function($q){
                                $q->where('gender','Perempuan');
                            })
                            ->count();
                    //muqaddimah lelaki
                    $maleMuqaddimah = Pretest::with('student')
                            ->with('muqaddimahPay')
                            ->where('pre_register_year',$pre_register_year)
                            ->where('type', 0)
                            ->whereHas('student', function($q) use ($ft_id, $dp_id){
                                $q->where('gender','Lelaki');
                                $q->where('ft_id', $ft_id);
                                $q->where('dp_id', $dp_id);
                            })
                            ->count();
                    //muqaddimah perempuan
                    $femaleMuqaddimah = Pretest::with('student')
                            ->with('muqaddimahPay')
                            ->where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('type', 0)
                            ->whereHas('student', function($q) use ($ft_id, $dp_id){
                                $q->where('gender','Perempuan');
                                $q->where('ft_id', $ft_id);
                                $q->where('dp_id', $dp_id);
                            })
                            ->count();
                    //muqaddimah transfer lelaki
                    $maleMuqaddimahTransfer = Pretest::where('pre_register_year',$pre_register_year)
                        ->where('payStatus',1)
                        ->where('type', 1)
                        ->with('student')
                        ->with('muqaddimahPay')
                        ->whereHas('student', function($q) use ($ft_id, $dp_id){
                            $q->where('gender','Lelaki');
                            $q->where('ft_id', $ft_id);
                            $q->where('dp_id', $dp_id);
                        })
                        ->count();
                    //muqaddimah transfer perempuan
                    $femaleMuqaddimahTransfer = Pretest::where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('type', 1)
                            ->with('student')
                            ->with('muqaddimahPay')
                            ->whereHas('student', function($q) use ($ft_id, $dp_id){
                                $q->where('gender','Perempuan');
                                $q->where('ft_id', $ft_id);
                                $q->where('dp_id', $dp_id);
                            })
                            ->count();
                    //total calon pilihan kedua
                    //lelaki
                    $maleSelectSecond = Pretest::with('student')
                            ->where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('second_ftId',$faculty->ft_id)
                            ->where('second_dpId',$department->dp_id)
                            ->whereHas('student', function($q){
                                $q->where('gender','Lelaki');
                            })
                            ->count();
                    //perempuant
                    $femaleSelectSecond = Pretest::with('student')
                            ->where('pre_register_year',$pre_register_year)
                            ->where('payStatus',1)
                            ->where('second_ftId',$faculty->ft_id)
                            ->where('second_dpId',$department->dp_id)
                            ->whereHas('student', function($q){
                                $q->where('gender','Perempuan');
                            })
                            ->count();
                    $statisticFirst[] = array(
                                    'faculty' => $facultyName,//$faculty->ft_id,
                                    'department' => $department->dp_short_name,
                                    'maleSelectFirst' => $maleSelectFirst,
                                    'femaleSelectFirst' => $femaleSelectFirst,
                                    'sumSelectFirst' => $maleSelectFirst+$femaleSelectFirst,
                                ); 
                    $statisticSecond[] = array(
                                    'faculty' => $facultyName,//$faculty->ft_id,
                                    'department' => $department->dp_short_name,
                                    'maleSelectSecond' => $maleSelectSecond,
                                    'femaleSelectSecond' => $femaleSelectSecond,
                                    'sumSelectSecond' => $maleSelectSecond+$femaleSelectSecond
                                ); 
                    $muqaddimahPayed[] = array(
                        'faculty' => $facultyName,
                        'department' => $department->dp_short_name,
                        'male' => $maleMuqaddimah,
                        'female' => $femaleMuqaddimah,
                        'sumMuqaddimah' => $maleMuqaddimah+$femaleMuqaddimah,
                    );
                    $muqaddimahTransferPayed[] = array(
                        'faculty' => $facultyName,
                        'department' => '',
                        'male' => $maleMuqaddimah,
                        'female' => $femaleMuqaddimah,
                        'sumMuqaddimahTransfer' => $maleMuqaddimahTransfer+$femaleMuqaddimahTransfer,
                    );
                } 
            }
            //$totalMuqaddimah = $totalMuqaddimah+$muqaddimahPayed[$index]['sumMuqaddimah'];
            //$totalMuqaddimahTransfer = $totalMuqaddimahTransfer+$muqaddimahTransferPayed[$index]['sumMuqaddimahTransfer'];
        }

        $collectionF = collect($statisticFirst);
        $sortedF = $collectionF->SortByDesc('sumSelectFirst');
        $statisticReturnF = $sortedF->values()->all();

        $collectionS = collect($statisticSecond);
        $sortedS = $collectionS->SortByDesc('sumSelectSecond');
        $statisticReturnS = $sortedS->values()->all();

        $collectionM = collect($muqaddimahPayed);
        $sortedM = $collectionM->SortByDesc('sumMuqaddimah');
        $statisticReturnM = $sortedM->values()->all();

        $collectionMT = collect($muqaddimahTransferPayed);
        $sortedMT = $collectionMT->SortByDesc('sumMuqaddimahTransfer');
        $statisticReturnMT = $sortedMT->values()->all();

        return [
            'first' => $statisticReturnF, 
            'second' => $statisticReturnS, 
            'muqaddimah' => $statisticReturnM,
            //'totalMuqaddimah' => $totalMuqaddimah,
            //'muqaddimahTransfer' => $statisticReturnMT,
            //'totalMuqaddimahTransfer' => $totalMuqaddimahTransfer,
        ];
    }
    //laporan senarai calon mengikut kelas periksa
    public function admissionTestRoomPretest(AdmissionTestRoomController $AdmissionTestRoomController, AdmissionRegisterController $AdmissionRegisterController, AdmissionPretestController $AdmissionPretestController)
    {
        $ar_id = $AdmissionRegisterController->currentAdmissionRegister()->id;
        $maleTestRooms = $AdmissionTestRoomController->currentTestRoomByGender($ar_id,1);
        foreach($maleTestRooms as $maleTestRoom){
            $atr_id = $maleTestRoom->id;
            $examinerCount = $AdmissionPretestController->admissionExaminerCount($atr_id);
            $count = $AdmissionPretestController->admissionExaminerCount($atr_id);
            $maleRoom[] = array(
                'room_id' => $maleTestRoom->id,
                'room_name' => $maleTestRoom->room_name,
                'count' => $count,
            );
        }
        $femaleTestRooms = $AdmissionTestRoomController->currentTestRoomByGender($ar_id,2);
        foreach($femaleTestRooms as $femaleTestRoom){
            $atr_id = $femaleTestRoom->id;
            $examinerCount = $AdmissionPretestController->admissionExaminerCount($atr_id);
            $count = $AdmissionPretestController->admissionExaminerCount($atr_id);
            $femaleRoom[] = array(
                'room_id' => $femaleTestRoom->id,
                'room_name' => $femaleTestRoom->room_name,
                'count' => $count,
            );
        }
        return ['male' => $maleRoom, 'female' => $femaleRoom];
    }
    // public function pretestRoomDownload($room_id)
    // {
    //     return Excel::download(new AdmissionPretestExport, $room_id.'.xlsx');
    // }
    public function admissionStudent(AdmissionRegisterController $admissionRegister)
    {
        //cari tahun penerimaan 
        $admission_register = $admissionRegister->currentAdmissionRegister();
        $ar_id = $admission_register->id;
        $ar_year = $admission_register->ar_year;

        $students = Pretest::with('student')
                            ->where('payStatus', 1)
                            ->where('pre_register_year', $ar_year)
                            ->orderBy('odrNumber')
                            ->paginate(20);
        return $students;
    }
    public function findAdmissionStudent(Request $request, AdmissionRegisterController $admissionRegister)
    {
        //cari tahun penerimaan 
        $admission_register = $admissionRegister->currentAdmissionRegister();
        $ar_id = $admission_register->id;
        $ar_year = $admission_register->ar_year;

        $query = $request->q;

        $students = Pretest::with('student')
                            ->where('payStatus', 1)
                            ->where('pre_register_year', $ar_year)
                            ->whereHas('student', function($q) use ($query){
                                $q->where('odrNumber', '=', $query);
                                $q->orWhere('firstname_rumi', 'LIKE', '%'.$query.'%');
                                $q->orWhere('lastname_rumi', 'LIKE', '%'.$query.'%');
                                $q->orWhere('t_studentname', 'LIKE', '%'.$query.'%');
                                $q->orWhere('t_studentlastname', 'LIKE', '%'.$query.'%');
                                $q->orWhere('firstname_jawi', 'LIKE', '%'.$query.'%');
                                $q->orWhere('lastname_jawi', 'LIKE', '%'.$query.'%');
                                //$q->orWhere('cityzen_id', 'LIKE', '%'.$query.'%');
                            })
                            ->orderBy('odrNumber', 'ASC')
                            ->paginate(20);
        return $students;
        // return [
        //     'query' => $query,
        //     'data' => $students,
        // ];
    }
    public function admissionStudentMuqaddimahForm($st_id, FakultyController $faculty, DepartmentController $department, AdmissionRegisterController $admissionRegister)
    {
        //cari tahun penerimaan 
        $admission_register = $admissionRegister->currentAdmissionRegister();
        $ar_id = $admission_register->id;
        $ar_year = $admission_register->ar_year;
        $student = Pretest::where('st_id', $st_id)
                            ->with('student')
                            ->where('pre_register_year', $ar_year)
                            ->first();
        if(MuqaddimahPay::where('p_id', $student->pre_id)->exists() == true){
            $muqaddimah = MuqaddimahPay::where('p_id', $student->pre_id)->first();
        }else{
            $muqaddimah = ['m_reciet' => '', 'm_paydate' => ''];
        }
        
        if($student->student->ft_id == NULL){
            $faculty = '';
            $department = '';
        }else{
            $ft = $faculty->show($student->student->ft_id);
            $faculty = $ft['ft_name'];
            $dp = $department->show($student->student->dp_id);
            $department = $dp['dp_name'];
        }
        return [
            'faculty' => $faculty,
            'department' => $department,
            'student' => $student, 
            'muqaddimah' => $muqaddimah
        ];
    }
    //jumlah mahasiswa bayar muqaddimah tahun sekarang
    public function currentStudentMuqaddimahPayed(AdmissionRegisterController $admissionRegisterController)
    {
        $year = $admissionRegisterController->currentAdmissionRegister();
        $classOne = Pretest::join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                            ->where('pretest.pre_register_year', $year->ar_year)->where('pretest.type', 0)->count();
        $transfer = Pretest::join('muqaddimah_pay', 'muqaddimah_pay.p_id', '=', 'pretest.pre_id')
                            ->where('muqaddimah_pay.m_academicyear', $year->ar_year)->where('type', 1)->count();
        return [
            'classOne' => $classOne,
            'transfer' => $transfer
        ];
    }

}