<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Register;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        //authorization
        //$this->authorize('isAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers = Register::all();
        return $registers;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //ข้อมูลปีการศึกษาระดับปริญญาตรี
    public function bachelorRegisterInfo()
    {
        return Register::where('p_id','0')
                ->orderBy('re_year', 'DESC')
                ->orderBy('re_term', 'DESC')
                ->get();
    }
    public function findBachelorRegister($re_id)
    {
        $register = Register::where('re_id',$re_id)
                    ->first();
        return $register;
    }

    //tahun pengajian yang sedang berlaku pengajian
    public function currentAcademicYear()
    {
        $register = Register::where('p_id', 0)->where('tu_id', 1)->first();
        return $register;
    }

    //kelas mahasiswa
    public function studentClass()
    {
        $currentYear = Register::where('p_id', 0)->where('tu_id', 1)->first();
        $class = [
            [
                'year' => $currentYear['re_year'],
                'class' => 1
            ],
            [
                'year' => $currentYear['re_year']-1,
                'class' => 2,
            ],
            [
                'year' => $currentYear['re_year']-2,
                'class' => 3,
            ],
            [
                'year' => $currentYear['re_year']-3,
                'class' => 4,
            ]
            ,
        ];
        return $class;
    }
}
