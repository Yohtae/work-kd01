<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UlangPay;
use App\Pretest;
use App\Student;
use App\Http\Controllers\API\AdmissionRegisterController;

class UlangPayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RegisterController $registerController)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, AdmissionRegisterController $admissionRegisterController)
    {
        $size = \sizeof($request->studentPaying);
        if($size != 0){

            $user_id = auth('api')->user()->id;
            $students = $request->studentPaying;
            $ar_year = $admissionRegisterController->currentAdmissionRegister()->ar_year;

            foreach($students as $student){
                $st_id = $student;
                $exist = UlangPay::where('up_year', $ar_year)->where('st_id', $st_id)->exists();
                if($exist == true){
                    
                }else{

                    //generate reciet number
                    $count = UlangPay::where('up_year', $ar_year)->count();
                    if($count == 0){
                        $up_number = 1;
                        $up_reciet = 'U'.substr($ar_year, 2).'/'.str_pad(1, 4, "0", STR_PAD_LEFT);
                    }else{
                        $allNumber = UlangPay::where('up_year', $ar_year)->get(['up_number']);

                        $collection = collect($allNumber);
                        $sorted = $collection->SortBy('up_number');
                        $numbers = $sorted->values()->all();

                        $i = 1;
                        foreach($numbers as $number){
                            $num = $number->up_number;
                            if($i != $num){
                                $up_number = $i;
                                $up_reciet = 'U'.substr($ar_year, 2).'/'.str_pad($up_number, 4, "0", STR_PAD_LEFT);
                                break;
                            }else{
                                $up_number = $i+1;
                                $up_reciet = 'U'.substr($ar_year, 2).'/'.str_pad($up_number, 4, "0", STR_PAD_LEFT);
                            }
                            $i++;
                        }
                    }

                    $pretest = Pretest::where('st_id', $st_id)->where('pre_register_year', $ar_year)->first(['type']);
                    $type = $pretest['type'];

                    if($type == 1){
                        $up_money = NULL;
                        $up_type = 2;
                    }else{
                        $up_money = 100;
                        $up_type = 1;
                    }

                    UlangPay::create([
                        'st_id' => $student,
                        'user_id' => $user_id,
                        'up_year' => $ar_year,
                        'up_money' => $up_money,
                        'up_number' => $up_number,
                        'up_reciet' => $up_reciet,
                        'up_type' => $up_type,
                    ]);
                }
            }
        }
        return [
            'status' => 'success',
            // 'request' => $request->all(),
            // 'number' => $numbers
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        
    }

    //mahasiswa yang sudah bayar daftar ulang pada tahun pengajian sekarang
    public function studentPayed(Request $request, RegisterController $registerController)
    {
        //example parameter
        $ft_id = $request->ft_id;
        $dp_id = $request->dp_id;
        $class = $request->class;

        //tahun pengajian sekarang
        $re_year = $registerController->currentAcademicYear()->re_year;

        $students = UlangPay::join('students', 'ulang_pays.st_id', '=', 'students.st_id')
                                ->where('ulang_pays.up_year', $re_year)
                                ->where('students.ft_id', $ft_id)
                                ->where('students.dp_id', $dp_id)
                                ->where('students.class', $class)
                                ->select([
                                    'ulang_pays.id','students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id'
                                ])
                                ->orderBy('student_id')
                                ->get();
        

        return $students;
    }
    //hapus mahasiswa yang sudah bayar daftar ulang
    public function ulangPayStudentDelete(Request $request)
    {
        foreach($request->studentDeleting as $student){
            UlangPay::where('id', $student)->delete();
        }
        return $request->all();
    }
    //daftar ulang untuk mahasiswa drop
    public function ulangPayStudentDrop(Request $request, AdmissionRegisterController $admissionRegisterController)
    {
        $user_id = auth('api')->user()->id;
        $ar_year = $admissionRegisterController->currentAdmissionRegister()->ar_year;

        //generate reciet number
        $count = UlangPay::where('up_year', $ar_year)->count();
        if($count == 0){
            $up_number = 1;
            $up_reciet = 'U'.substr($ar_year, 2).'/'.str_pad(1, 4, "0", STR_PAD_LEFT);
        }else{
            $allNumber = UlangPay::where('up_year', $ar_year)->get(['up_number']);

            $collection = collect($allNumber);
            $sorted = $collection->SortBy('up_number');
            $numbers = $sorted->values()->all();

            $i = 1;
            foreach($numbers as $number){
                $num = $number->up_number;
                if($i != $num){
                    $up_number = $i;
                    $up_reciet = 'U'.substr($ar_year, 2).'/'.str_pad($up_number, 4, "0", STR_PAD_LEFT);
                    break;
                }else{
                    $up_number = $i+1;
                    $up_reciet = 'U'.substr($ar_year, 2).'/'.str_pad($up_number, 4, "0", STR_PAD_LEFT);
                }
                $i++;
            }
        }

        //check sudah daftar atau belum
        $exist = UlangPay::where('st_id', $request->st_id)->where('up_year', $ar_year)->exists();
        if($exist == true){
            $status = 'exist';
        }else{
            UlangPay::create([
                'st_id' => $request->st_id,
                'user_id' => $user_id,
                'up_year' => $ar_year,
                'up_money' => null,
                'up_number' => $up_number,
                'up_reciet' => $up_reciet,
                'up_type' => 3,
            ]);
            Student::where('st_id', $request->st_id)
                        ->update([
                            'class' => $request->class
                        ]);
            $status = 'success';
        }

        return [
            'status' => $status,
        ];
    }
    //mahasiswa drop yang sudah bayar daftar ulang pada tahun ini
    public function UlangPayStudentDropList(AdmissionRegisterController $admissionRegisterController)
    {
        $ar_year = $admissionRegisterController->currentAdmissionRegister()->ar_year;    
        $students = UlangPay::where('up_year', $ar_year)
                        ->join('students', 'ulang_pays.st_id', '=', 'students.st_id')
                        ->where('up_type', 3)
                        ->select([
                            'ulang_pays.id','students.st_id','students.firstname_rumi','students.lastname_rumi','students.firstname_jawi','students.lastname_jawi','students.student_id'
                        ])
                        ->get();
        return $students;
    }
    //membatal mahasiswa drop
    public function UlangPayStudentDropDelete($id)
    {
        $payment = UlangPay::find($id);
        $payment->delete();
        return ['status' => 'success'];
    }
}
