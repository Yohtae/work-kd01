<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionRegister extends Model
{

    protected $fillable = ['ar_year','ar_start_date','ar_end_date','user_id','status','ar_register_cost','ar_muqaddimah_cost'];

    public function admissionTestRoom()
    {
        return $this->hasMany('App\AdmissionTestRoom');
    }

    // public function user()
    // {
    //     return $this->belongsTo('App\User','user_id','id');
    // }

}
