<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MuqaddimahPay extends Model
{
    protected $table = 'muqaddimah_pay';
    protected $fillable = ['p_id', 'st_id', 'm_academicyear', 'm_paydate', 'm_money', 'm_reciet', 'm_number','user_id'];
    public function pretest()
    {
        return $this->hasMany('App\Pretest');
    }
}
