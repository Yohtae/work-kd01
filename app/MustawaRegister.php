<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MustawaRegister extends Model
{
    protected $table = 'mustawa_register';
    
    protected $fillable = [
        'mustawadata_id','payMoney','register_date','reciet','reciet_code','st_id','learningGroup','learningStatus','year'
    ];
}
