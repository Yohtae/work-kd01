<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user){
            return $user->type == '1';
        }); 

        Gate::define('isFinance', function($user){
            return $user->type == '2';
        });

        Gate::define('isRegistration', function($user){
            return $user->type == '3';
        });

        Gate::define('isAcademic', function($user){
            return $user->type == '4';
        });

        Gate::define('isFacultyEmployee', function($user){
            return $user->type == '5';
        });

        Passport::routes();
    }
}
