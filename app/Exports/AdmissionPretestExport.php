<?php

namespace App\Exports;

use App\AdmissionPretest;
use Maatwebsite\Excel\Concerns\FromCollection;

class AdmissionPretestExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AdmissionPretest::all();
    }
}
