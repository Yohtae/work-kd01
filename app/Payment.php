<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = ['sr_id','st_id' ,'user_id' ,'pay_date','money','penalty','reciet_code','reciet_number','academicYear'];
    
}
