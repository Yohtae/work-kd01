<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRegisterExam extends Model
{
    protected $table = 'student_register_exam';
}
