<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterSubject extends Model
{
    protected $table = 'registerSubject';
}
