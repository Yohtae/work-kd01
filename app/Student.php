<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    protected $fillable = [
        'student_id','firstname_rumi','lastname_rumi','familyname_rumi',
        'ft_id','dp_id','income_year','class','register_date','status',
        'firstname_jawi','lastname_jawi','familyname_jawi','t_studentname',
        't_studentlastname','gender','birdth_date','father_name','father_lastname',
        'father_job','mother_name','mother_lastname','mother_job','familyStatus',
        'disease','telephone','email','t_village_name','house_number','place',
        't_road','t_province','t_district','t_subdistrict','post','ibtidai_graduate_year',
        'ibtidaiVillage','ibtidai_graduate','mutawasit_graduate_year','mutawassitVillage',
        'mutawasit_graduate','sanawi_graduate_year','sanawiVillage','sanawi_graduate',
        'down_graduate','down_graduate_year','first_highschool_graduate','first_highschool_graduate_year',
        'second_highschool_graduate','second_highschool_graduate_year','melayu_lang_skill',
        'arab_lang_skill','ingris_lang_skill','thai_lang_skill','certificate',
        'citizen_book','id_book','transcript','father_idcard','father_citizenbook','mother_idcard',
        'mother_citizenbook','photo','program','numbers_of_siblings','sibling_number','marital_status',
        'have_income_from','public_benefit_activities','skills'
    ];

    public function studentRegister(){
        return $this->hasMany('App\StudentRegister')->orderBy('student_id','ASC');
    }

    public function studentSubject(){
        return $this->hasMany('App\StudentSubject');
    }

    public function pretest()
    {
        return $this->hasOne('App\Pretest');
    }
    public function admissionTestRoom()
    {
        return $this->hasOne('App\AdmissionPretest');
    }
    public function faculty()
    {
        return $this->belongsTo('App\Fakulty', 'ft_id', 'ft_id');
    }
}
