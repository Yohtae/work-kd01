<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pretest extends Model
{
    protected $table = 'pretest';

    protected $fillable = ['testClass','testNumber','odrNumber','payStatus','muqaddimah_pay_status','user_id'];

    public function student()
    {
        return $this->belongsTo('App\Student','st_id','st_id')
                    ->select(array(
                        'st_id',
                        'cityzen_id',
                        'firstname_rumi',
                        'lastname_rumi',
                        'firstname_jawi',
                        'lastname_jawi',
                        'gender',
                        'ft_id',
                        'dp_id',
                        'class'
                    ));
    }

    public function muqaddimahPay()
    {
        return $this->belongsTo('App\MuqaddimahPay', 'pre_id', 'p_id');
    }

}
