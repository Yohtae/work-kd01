<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionTestRoom extends Model
{

    protected $table = 'admission_test_rooms';
    
    protected $fillable = ['ar_id', 'room_name', 'room_gender', 'room_description'];

    public function admissionRegister()
    {
        return $this->belongsTo('App\AdmissionRegister','ar_id','id');
    }

    public function admissionPretest()
    {
        return $this->hasMany('App\AdmissionPretest');
    }

}
