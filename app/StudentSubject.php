<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSubject extends Model
{
    Protected $table = 'studentsubject';

    Protected $fillable = ['ft_id','dp_id','income_year','class'];

    public function student(){
        return $this->belongsTo('App\Student','st_id','st_id')
                    ->select(array(
                        'st_id',
                        'student_id',
                        'firstname_rumi',
                        'lastname_rumi',
                        'firstname_jawi',
                        'lastname_jawi')
                    );
    }
    public function subject(){
        return $this->belongsTo('App\Subject','s_id','s_id');
    }
    public function teacher(){
        return $this->belongsTo('App\Teacher','t_id','t_id');
    }
}
