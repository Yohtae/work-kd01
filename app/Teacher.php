<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    public function studentSubject(){
        return $this->hasMany('App\StudentSubject');
    }
}
