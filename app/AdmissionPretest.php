<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionPretest extends Model
{
    
    protected $fillable = ['atr_id','st_id','seat_number','note'];

    public function admissionTestRoom()
    {
        return $this->belongsTo('App\AdmissionTestRoom','atr_id','id');
    }

    public function student()
    {
        return $this->belongsTo('App\Student','st_id','st_id')
                    ->select(array(
                        'st_id',
                        'student_id',
                        'firstname_rumi',
                        'lastname_rumi',
                        'firstname_jawi',
                        'lastname_jawi')
                    );
    }

}
