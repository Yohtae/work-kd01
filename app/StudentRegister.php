<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRegister extends Model
{
    protected $table = 'student_register';

    protected $fillable = ['pay_status'];

    public function student()
    {
        return $this->belongsTo('App\Student','st_id','st_id')
                    ->select(
                        array(
                            'st_id',
                            'student_id',
                            'ft_id',
                            'dp_id',
                            'firstname_rumi',
                            'lastname_rumi',
                            'firstname_jawi',
                            'lastname_jawi',
                            'cityzen_id'
                        ));
    }
}
