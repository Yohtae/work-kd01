<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UlangPay extends Model
{
    protected $fillable = ['st_id','user_id','up_year','up_money','up_number','up_reciet','up_type'];
}
