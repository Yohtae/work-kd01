using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Jisdaorg_system
{
    #region Studentsubject
    public class Studentsubject
    {
        #region Member Variables
        protected int _ss_id;
        protected int _s_id;
        protected int _st_id;
        protected int _t_id;
        protected int _ss_term;
        protected string _ss_year;
        protected string _ss_score;
        #endregion
        #region Constructors
        public Studentsubject() { }
        public Studentsubject(int s_id, int st_id, int t_id, int ss_term, string ss_year, string ss_score)
        {
            this._s_id=s_id;
            this._st_id=st_id;
            this._t_id=t_id;
            this._ss_term=ss_term;
            this._ss_year=ss_year;
            this._ss_score=ss_score;
        }
        #endregion
        #region Public Properties
        public virtual int Ss_id
        {
            get {return _ss_id;}
            set {_ss_id=value;}
        }
        public virtual int S_id
        {
            get {return _s_id;}
            set {_s_id=value;}
        }
        public virtual int St_id
        {
            get {return _st_id;}
            set {_st_id=value;}
        }
        public virtual int T_id
        {
            get {return _t_id;}
            set {_t_id=value;}
        }
        public virtual int Ss_term
        {
            get {return _ss_term;}
            set {_ss_term=value;}
        }
        public virtual string Ss_year
        {
            get {return _ss_year;}
            set {_ss_year=value;}
        }
        public virtual string Ss_score
        {
            get {return _ss_score;}
            set {_ss_score=value;}
        }
        #endregion
    }
    #endregion
}