<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionPretestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_pretests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('atr_id')->comment('admission_test_rooms');
            $table->foreign('atr_id')->references('id')->on('admission_test_rooms');
            $table->integer('st_id')->comment('students');
            $table->integer('seat_number')->comment('Nobor kursi');
            $table->string('note')->comment('Catatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_pretests');
    }
}
