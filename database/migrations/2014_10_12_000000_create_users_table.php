<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('lastname');
            $table->string('idcard')->unique();
            $table->string('telephone')->nullable();
            $table->integer('type')->comment('1:admin 2:finance 3:registration 4:academic 5:faculty employee');
            $table->unsignedBigInteger('ft_id')->nullable();
            $table->foreign('ft_id')->references('ft_id')->on('fakultys');
            $table->unsignedBigInteger('dp_id')->nullable();
            $table->foreign('dp_id')->references('dp_id')->on('departments');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('username')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
