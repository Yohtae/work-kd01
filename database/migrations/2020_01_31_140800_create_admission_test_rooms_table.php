<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionTestRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_test_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ar_id')->comment('admission_registers');
            $table->foreign('ar_id')->references('id')->on('admission_registers');
            $table->char('room_name', 50)->comment('nama bilik');
            $table->integer('room_gender')->comment('1:Lelaki 2:Perempuan');
            $table->string('room_description')->comment('Pernyataan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_test_rooms');
    }
}
