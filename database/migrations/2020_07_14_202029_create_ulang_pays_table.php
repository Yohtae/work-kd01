<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUlangPaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ulang_pays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('st_id')->index('st_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('up_year');
            $table->integer('up_money')->nullable();
            $table->integer('up_number');
            $table->string('up_reciet', 10);
            $table->integer('up_type')->comment('1: normal 2:transfer 3:drop');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ulang_pays');
    }
}
