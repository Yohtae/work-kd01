<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_registers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('ar_year', 4)->unique()->comment('Tahun pengajian');
            $table->date('ar_start_date')->comment('Tarikh mulai daftar');
            $table->date('ar_end_date')->comment('Tarikh akhir daftar');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('status')->comment('1:active 2:unactive');
            $table->float('ar_register_cost')->comment('harga daftar');
            $table->float('ar_muqaddimah_cost')->comment('harga muqaddimah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_registers');
    }
}
