<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
        <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
            <div>
            <p class="app-sidebar__user-name">{{ Auth::user()->name }} {{ Auth::user()->lastname }}</p>
            <p class="app-sidebar__user-designation">PENGUNA</p>
            </div>
        </div>
        <ul class="app-menu">
            <li>
                <router-link to="/dashboard" class="app-menu__item">
                    <i class="app-menu__icon fa fa-dashboard"></i>
                    <span class="app-menu__label">Dashboard</span>
                </router-link>
            </li>
            <li>
                <router-link to="/profile" class="app-menu__item">
                    <i class="app-menu__icon fa fa-pie-chart"></i>
                    <span class="app-menu__label">Profile</span>
                </router-link>
            </li>
            
            @if(auth()->user()->can('developer'))
            <li>
                <router-link to="/developer" class="app-menu__item">
                    <i class="app-menu__icon fa fa-code"></i>
                    <span class="app-menu__label">Developer</span>
                </router-link>
            </li>
            @endif

            @if(auth()->user()->can('setting'))
                <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-cog"></i><span class="app-menu__label">Penetapan / Setting</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                        <li><router-link to="/settingNim" class="treeview-item"><i class="icon fa fa-circle-o"></i> Upload NIM</router-link></li>
                    </ul>
                </li>
            @endif
            
            @if(auth()->user()->can('user'))
            <li>
                <router-link to="/user" class="app-menu__item">
                    <i class="app-menu__icon fa fa-users"></i>
                    <span class="app-menu__label">Penguna</span>
                </router-link>
            </li>
            @endif

            @if(auth()->user()->can('admission'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-registered"></i><span class="app-menu__label">Penerimaan baru</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><router-link to="/admissionSetting" class="treeview-item"><i class="icon fa fa-circle-o"></i> Tetapan</router-link></li>
                    <li><router-link to="/admissionList" class="treeview-item"><i class="icon fa fa-circle-o"></i> Daftar calon</router-link></li>
                    <li><router-link to="/admissionDocument" class="treeview-item"><i class="icon fa fa-circle-o"></i> Print & Export</router-link></li>
                    <li><router-link to="/admissionReport" class="treeview-item"><i class="icon fa fa-circle-o"></i> Laporan</a></li>
                </ul>
            </li>
            @endif

            @if(auth()->user()->can('student'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-graduation-cap"></i><span class="app-menu__label">Mahasiswa</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><router-link to="/studentInfo" class="treeview-item"><i class="icon fa fa-circle-o"></i> Data</router-link></li>
                    <li><router-link to="/studentReport" class="treeview-item"><i class="icon fa fa-circle-o"></i> Laporan</router-link></li>
                </ul>
            </li>
            @endif

            @if(auth()->user()->can('payment'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-money"></i><span class="app-menu__label">Bayaran</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><router-link to="/PaymentMuqaddimah" class="treeview-item"><i class="icon fa fa-circle-o"></i> Muqaddimah</router-link></li>
                    <li><router-link to="/PaymentUlang" class="treeview-item"><i class="icon fa fa-circle-o"></i> Daftar ulang</router-link></li>
                    <li><router-link to="/PaymentYuran" class="treeview-item"><i class="icon fa fa-circle-o"></i> Yuran</router-link></li>
                    <li><router-link to="/PaymentExam" class="treeview-item"><i class="icon fa fa-circle-o"></i> Ujian</a></li>
                    <li><router-link to="/PaymentMustawa" class="treeview-item"><i class="icon fa fa-circle-o"></i> Mustawa</a></li>
                    <li><router-link to="/PaymentDur" class="treeview-item"><i class="icon fa fa-circle-o"></i> Dur</a></li>
                    <li><router-link to="/PaymentDrop" class="treeview-item"><i class="icon fa fa-circle-o"></i> Drop</a></li>
                </ul>
            </li>
            @endif

            @if(auth()->user()->can('export'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-money"></i><span class="app-menu__label">Print & Export</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><router-link to="/studentClassroom" class="treeview-item"><i class="icon fa fa-circle-o"></i> Mahasiswa</router-link></li>
                </ul>
            </li>
            @endif

            {{-- @if(Gate::check('isAdmin') || Gate::check('isFacultyEmployee')) --}}
            @if(auth()->user()->can('study_result'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-list-alt"></i><span class="app-menu__label">Hasil studi</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><router-link to="/ScoreRecordByClass" class="treeview-item"><i class="icon fa fa-circle-o"></i> Isi markah mengikut kelas</router-link></li>
                    <li><router-link to="/ScoreRecordPerson" class="treeview-item"><i class="icon fa fa-circle-o"></i> Isi markah mengikut mahasiswa</router-link></li>
                    <li><router-link to="/ScoreReportByClass" class="treeview-item"><i class="icon fa fa-circle-o"></i> Hasil studi mengikut kelas</a></li>
                </ul>
            </li>
            @endif

            @if(auth()->user()->can('student_attendance'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-check-square-o"></i><span class="app-menu__label">Kehadiran mahasiswa</span><i class="treeview-indicator fa fa-angle-right"></a></i>
                <ul class="treeview-menu">
                    <li><router-link to="/attendanceChecker" class="treeview-item"><i class="icon fa fa-circle-o"></i> Pencatat</router-link></li>
                    <li><router-link to="/attendanceReport" class="treeview-item"><i class="icon fa fa-circle-o"></i> Laporan</a></li>
                </ul>
            </li>
            @endif
            
            @if(auth()->user()->can('special'))
            <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-wrench"></i><span class="app-menu__label">special</span><i class="treeview-indicator fa fa-angle-right"></i></a>
                <ul class="treeview-menu">
                    <li><router-link to="/updateRegisterStudent" class="treeview-item"><i class="icon fa fa-circle-o"></i> Pendaftaran</router-link></li>
                    <li><router-link to="/updateRegisterSubject" class="treeview-item"><i class="icon fa fa-circle-o"></i> Mata kuliah</a></li>
                </ul>
            </li>
            @endif

            <li>
                <a class="app-menu__item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class=" app-menu__icon fa fa-sign-out" aria-hidden="true"></i>
                    <span class="app-menu__label">Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>


        </ul>
</aside>