
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    @include('components.style')
  </head>
  <body class="app sidebar-mini">
    <div id="app">
        <!-- Navbar -->
        @include('components.header')

        <!-- Sidebar menu -->
        @include('components.sidebar')

        <!-- Main content -->
        <router-view></router-view>
        <vue-progress-bar></vue-progress-bar>
        <!-- \.Main content -->
    </div>

    @auth
      <script>
        window.user = @json(auth()->user())
      </script>
    @endauth

    <script>
        @auth
            window.Permissions = @json(auth()->user()->allPermissions);
        @else
            window.Permissions = [];
        @endauth
    </script>

    <!-- script -->
    <script type="text/javascript" src="/js/app.js?version=09092020"></script>
    <!-- <script src="vali-admin/docs/js/popper.min.js"></script>
    <script src="vali-admin/docs/js/bootstrap.min.js"></script> -->
    <script src="vali-admin/docs/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <!-- <script src="vali-admin/docs/js/plugins/pace.min.js"></script> -->

  </body>
</html>