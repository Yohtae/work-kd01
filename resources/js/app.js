require('./bootstrap');
window.Vue = require('vue');
window._ = require('lodash');
window.fire = new Vue();
//window.fire = new Vue();
import VueRouter from 'vue-router'
import { Form, HasError, AlertError } from 'vform'
import VueProgressBar from 'vue-progressbar'
// ES6 Modules or TypeScript
//import Swal from 'sweetalert2'
import Gate from "./Gate";
import LongdoMap from 'longdo-map-vue' 
import VueHtmlToPaper from 'vue-html-to-paper';
import moment from 'moment'
import permissions from "./mixins/Permissions.js";

//------------------------------------filtering-------------------------------------------------
//filtering 
Vue.filter('upText', function(text){
  return text.charAt(0).toUpperCase() + text.slice(1)
});

Vue.filter('thaiDateTime', function(created){
  return moment(created).locale('th-TH').format('Do MMMM YYYY, h:mm:ss a');
});

Vue.filter('thaiTime', function(created){
  return moment(created).format('H:mm:ss');
});

Vue.filter('thaiDate', function(date){
  return moment(date).format('DD/MM/YYYY');
});


//------------------------------------vue html to paper-----------------------------------------

const options = {
    name: '_blank',
    specs: [
      'fullscreen=yes',
      'titlebar=yes',
      'scrollbars=yes'
    ],
    styles: [
      'css/print.css',
    ]
  }

Vue.use(VueHtmlToPaper, options);

//------------------------------------Longdo map-----------------------------------------
Vue.use(LongdoMap, {     
    load: {         
         apiKey: 'ae777ac6daee8c874f82ba766cf96caa'     
    } 
})

//------------------------------------sweet alert2-----------------------------------------
const Swal = require('sweetalert2')
const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  window.toast = toast
  window.swal = Swal

//------------------------------------Permission------------------------------------------- 
Vue.mixin(permissions);
  

//------------------------------------vue form-----------------------------------------
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//------------------------------------vue router-----------------------------------------
Vue.use(VueRouter)

let routes = [
    { path: '/', component: require('./components/Dashboard.vue').default },
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '/developer', component: require('./components/Developer').default },
    { path: '/user', component: require('./components/user.vue').default },
    { path: '/updateRegisterStudent', component: require('./components/special/updateRegisterStudent.vue').default },
    { path: '/updateRegisterSubject', component: require('./components/special/UpdateStudentSubject.vue').default },
    { path: '/attendanceChecker', component: require('./components/attendance/AttendanceChecker.vue').default },
    { path: '/attendanceReport', component: require('./components/attendance/AttendanceReport.vue').default },
    { path: '/ScoreRecordByClass', component: require('./components/studyResult/ScoreRecordByClass.vue').default },
    { path: '/ScoreRecordPerson', component: require('./components/studyResult/ScoreRecordByPerson.vue').default },
    { path: '/ScoreReportByClass', component: require('./components/studyResult/ScoreReportByClass.vue').default },
    { path: '/admissionSetting', component: require('./components/admission/admissionSetting.vue').default },
    { path: '/admissionList', component: require('./components/admission/admissionList.vue').default },
    { path: '/admissionReport', component: require('./components/admission/admissionReport.vue').default },
    { path: '/admissionDocument', component: require('./components/admission/admissionDocumentHome.vue').default },
    { path: '/admissionDocumentHome', component: require('./components/admission/admissionDocumentHome.vue').default },
    { path: '/admissionDocumentBiodata', component: require('./components/admission/admissionDocumentBiodata.vue').default },
    { path: '/confirmingRegister', name: 'confirmingRegister', component: require('./components/admission/confirmingRegister.vue').default },
    { path: '/takePhoto', name: 'takePhoto', component: require('./components/admission/takePhoto.vue').default },
    //payment
    { path: '/PaymentUlang', component: require('./components/payment/Ulang.vue').default },
    { path: '/PaymentUlangForm', name: 'PaymentUlangForm', component: require('./components/payment/PaymentUlangForm.vue').default, props: true,},
    { path: '/PaymentMuqaddimah', component: require('./components/payment/Muqaddimah.vue').default },
    { path: '/PaymentUlangDrop', component: require('./components/payment/PaymentUlangDrop.vue').default },
    { path: '/PaymentDropList', component: require('./components/payment/PaymentDropList.vue').default },
    { path: '/PaymentMustawa', component: require('./components/payment/Mustawa.vue').default },
    { path: '/MustawaForm', name: 'MustawaForm', component: require('./components/payment/MustawaForm.vue').default, props: true, },
    { path: '/PaymentYuran', component:require('./components/payment/Yuran.vue').default },
    { path: '/YuranForm', name: 'YuranForm', component: require('./components/payment/YuranForm.vue').default, props: true, },

    //report
    { path: '/studentClassroom', component: require('./components/export/Mahasiswa.vue').default },
    //setting
    { path: '/settingNim', component: require('./components/setting/settingNim.vue').default },
    //student
    { path: '/studentInfo', component: require('./components/student/StudentInfo.vue').default },
    { path: '/studentReport', component: require('./components/student/StudentReport.vue').default },
    { path: '/StudentEdit', name: 'StudentEdit', component: require('./components/student/StudentEdit.vue').default, props: true,},
    { path: '/StudentTakePhoto', name: 'StudentTakePhoto', component: require('./components/student/StudentTakePhoto.vue').default, props: true },
    { path: '/StudentData', name: 'StudentData', component: require('./components/student/StudentData.vue').default, props: true },
]

//------------------------------------vue passport-----------------------------------------
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

//------------------------------------filter-----------------------------------------
Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});

//------------------------------------Progressbar-----------------------------------------
Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
})

//------------------------------------pagination------------------------------------------
Vue.component('pagination', require('./components/partial/PaginationComponent.vue').default);


//------------------------------------Gate (ACL)------------------------------------------------
Vue.prototype.$gate = new Gate(window.user);

//------------------------------------Not found component------------------------------------------------
Vue.component('not-found', 
    require('./components/NotFound.vue').default);

const app = new Vue({
    el: '#app',
    router,
    data:{
        //userInfo: window.user,
    },
});
