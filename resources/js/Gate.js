export default class Gate{
    
    constructor(user){
        this.user = user;
    }

    isAdmin(){
        return this.user.type == '1';
    }

    isFinance(){
        return this.user.type == '2';
    }

    isRegistration(){
        return this.user.type == '3';
    }

    isAcademic(){
        return this.user.type == '4';
    }

    isFacultyEmployee(){
        return this.user.type == '5';
    }

    isAdminOrRegistration(){
        if(this.user.type == '1' || this.user.type == '3'){
            return true;
        }
    }

    isAdminOrFacultyEmployee(){
        if(this.user.type == '1' || this.user.type == '5'){
            return true;
        }
    }

}